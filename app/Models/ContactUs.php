<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContactUs extends Model
{
    protected $table='contact_uses';
    protected $fillable=['id','created_at','updated_at','company','address','phone','mobile','email','map','facebook','twitter','instagram','gplus','youtube','linked_in','image'];
}
