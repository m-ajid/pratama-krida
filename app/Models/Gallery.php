<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $table='galleries';
    protected $fillable=['id','created_at','updated_at','title','label','image','description'];

    public static function album($is_album=null){
        $res=static::orderBy('created_at','desc');
        if ($is_album==true){
            $res=$res->where('label','<>','Other');
        }else{
            $res=$res->where('label','=','Other');
        }
        return $res;
    }

    public static function imageByAlbum($album_name){
        return static::where('label','=',$album_name);
    }
}
