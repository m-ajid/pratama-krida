<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $table='services';
    protected $fillable=['id','created_at','updated_at','title','image','description','short_description','category'];
}
