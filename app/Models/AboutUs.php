<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AboutUs extends Model
{
    protected $table='about_uses';
    protected $fillable=['id','short_description','image','description','created_at','updated_at'];
}
