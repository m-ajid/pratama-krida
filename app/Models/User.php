<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\UserInterface;

use Cartalyst\Sentinel\Users\EloquentUser as Model;
use DB;
use Carbon\Carbon;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model  implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;
    /**
     * Default password.
     *
     * @var string 
     */
    
    protected $table='users';
    /**
     * {@inheritDoc}
     */
    protected $fillable = [
        'email', 'password', 'permissions', 'first_name', 'last_name', 'avatar', 'is_admin', 'skin'
    ];

    /**
     * {@inheritDoc}
     */
    protected $hidden = [
        'password',
    ];

    /**
     * Return user's query for Datatables.
     *
     * @param  bool|null $isAdmin
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function datatables($isAdmin = null,$search = null)
    {
        $res = static::select('id', 'email', 'first_name', 'last_name');

        if (is_bool($isAdmin)) {
            $res=$res->where('is_admin', $isAdmin);
        }

        return $res;
    }
}
