<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Testimony extends Model
{
    protected $table='testimonies';
    protected $fillable=['id','created_at','updated_at','name','company','image','description'];
}
