<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UploadTemp extends Model
{
     protected $table='upload_temps';
    protected $fillable=['id','user_id','image','created_at','updated_at'];
}
