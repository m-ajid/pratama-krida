<?php
/*
| My Helpers.
|
| @author Ajid Muhamad <mail.ajid13@gmail.com>
*/

if (! function_exists('user_info')) {
    /**
     * Get logged user info.
     * 
     * @param  string $column
     * @return mixed
     */
    function user_info($column = null)
    {
        if ($user = Sentinel::check()) {
            if (is_null($column)) {
                return $user;
            }

            if ('full_name' == $column) {
                return user_info('first_name').' '.user_info('last_name');
            }

            if ('role' == $column) {
                return user_info()->roles[0];
            }

            return $user->{$column};
        }

        return null;
    }
}
if (! function_exists('link_to_images')) {
    /**
     * Generates link to avatar.
     * 
     * @param  null|string $path
     * @return string
     */
    function link_to_images($path = null)
    {
        // if (is_null($path)) {
        //     return 'http://lorempixel.com/128/128/';
        // }

        return asset('storage/images/').'/'.trim($path, '/');
    }
}

if (! function_exists('link_to_carousels')) {
    /**
     * Generates link to avatar.
     * 
     * @param  null|string $path
     * @return string
     */
    function link_to_carousels($path = null)
    {
        // if (is_null($path)) {
        //     return 'http://lorempixel.com/128/128/';
        // }

        return asset('storage/images/carousel').'/'.trim($path, '/');
    }
}

if (! function_exists('link_to_avatar')) {
    /**
     * Generates link to avatar.
     * 
     * @param  null|string $path
     * @return string
     */
    function link_to_avatar($path = null)
    {
        if (is_null($path) || ! file_exists(avatar_path($path))) {
            return 'http://lorempixel.com/128/128/';
        }

        return asset('storage/avatars').'/'.trim($path, '/');
    }
}
if (! function_exists('link_to_gallery')) {
    /**
     * Generates link to avatar.
     * 
     * @param  null|string $path
     * @return string
     */
    function link_to_gallery($album_name = null,$file_name)
    {
        // if (is_null($file_name) || ! file_exists(avatar_path($file_name))) {
        //     return 'http://lorempixel.com/128/128/';
        // }

        return asset('storage/images/gallery').'/'.trim($album_name, '/').'/'.trim($file_name, '/');
    }
}
if (! function_exists('avatar_path')) {
    /**
     * Generates avatars path.
     * 
     * @param  null|string $path
     * @return string
     */
    function avatar_path($path = null)
    {
        $link = public_path('storage/avatars');

        if (is_null($path)) {
            return $link;
        }

        return $link.'/'.trim($path, '/');
    }
}
if (! function_exists('images_path')) {
    /**
     * Generates avatars path.
     * 
     * @param  null|string $path
     * @return string
     */
    function images_path($path = null)
    {
        $link = public_path('storage/images');

        if (is_null($path)) {
            return $link;
        }

        return $link.'/'.trim($path, '/');
    }
}
if (! function_exists('gallery_path')) {
    /**
     * Generates avatars path.
     * 
     * @param  null|string $path
     * @return string
     */
    function gallery_path($path = null)
    {
        $link = public_path('storage/images/gallery');

        if (is_null($path)) {
            return $link;
        }

        return $link.'/'.trim($path, '/');
    }
}

if (! function_exists('datatables')) {
    /**
     * Shortcut for Datatables::of().
     * 
     * @param  mixed $builder
     * @return mixed
     */
    function datatables($builder)
    {
        return Datatables::of($builder);
    }
}

if (! function_exists('my_datetime')) {
    /**
     * Generate new datetime from configured format datetime.
     * 
     * @param  string $datetime
     * @return string
     */
    function my_datetime($datetime)
    {
        return date(env('APP_DATE_FORMAT', 'd M Y H:i:s'), strtotime($datetime));
    }
}

if (! function_exists('my_form_title')) {
    /**
     * Generate title for form.
     * 
     * @param  int    $id
     * @return string
     */
    function my_form_title($id = 0)
    {
        return $id > 0 ? 'edit' : 'add';
    }
}


if (! function_exists('number_to_words')) {
    /**
     * Get logged user info.
     * 
     * @param  string $column
     * @return mixed
     */
    function number_to_words($number=null)
    {
        $is_negative='';
        if($number<0) {
            $is_negative = 'minus ';
        }

        $number = abs($number);
        $length=strlen($number);
        if($number<1000){
            if(substr($number,0,1)==0){
                $temp='';
            }else{
                $temp=$number;
            }
        }
        elseif ($number< 1000000) {
            if (substr($number, 1,-3)!=000 || substr($number, 1,-3)!=0000){
                $temp=substr($number,0,-3).' Ribu '.number_to_words(substr($number,-3));
            }else{
                $temp=substr($number,0,-3).' Ribu';
            } 
        }
        elseif ($number<1000000000) {
            if (substr($number, 1,-3)!=000 || substr($number, 1,-3)!=0000){
                $temp=substr($number,0,-6).' Juta '.number_to_words(substr($number,-6));
            }else{
                $temp=substr($number,0,-6).' Juta';
            }
        }
        elseif ($number<1000000000000) {
            if (substr($number, 1,-6)!=000 || substr($number, 1,-6)!=0000){
                $temp=substr($number,0,-9).' Milyar '.number_to_words(substr($number,-9));
            }else{
                $temp=substr($number, 0, -9).' Milyar';
            }
        }elseif($number>=1000000000000) {
            if (substr($number, 1,-3)!=000 || substr($number, 1,-3)!=0000){
                $temp=substr($number,0,-12).' Trilyun '.number_to_words(substr($number,-12));
            }else{
                $temp=substr($number, 0, -12).' Trilyun';
            }
        }
        
        return $is_negative.trim($temp);
    }
}





