<?php

namespace App\Helpers;

use Sentinel;

class HasAccess
{
    /**
     * Check if user has access.
     * 
     * @param  array|string  $permissions
     * @param  bool          $any
     * @return bool
     */
    public static function hasAccess($permissions, $any = false)
    {
        $method = 'hasAccess';
        if ($any) {
            $method = 'hasAnyAccess';
        }

        if ((bool) user_info('role')->is_super_admin) {
            return true;
        }

        return Sentinel::check()->{$method}($permissions);
    }
}
