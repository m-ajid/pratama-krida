<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Carousel;
use App\DataTables\CarouselDataTables as Datatables;
use DB;
class CarouselController extends Controller
{
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Datatables $dataTable)
    {
        return $dataTable->render('backend.carousel.index');
    }

    public function create()
    {
        return $this->createEdit();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->storeUpdate($request);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return $this->createEdit($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->storeUpdate($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $carousel = Carousel::find($id);
            $this->deleteimage($carousel->image);
            $carousel->delete();
            DB::commit();
            return redirect(route('admin.carousel'));
        } catch (Exception $e) {
            DB::rollback();
        }

        return redirect(route('admin.carousel'));
    }

    /**
     * Handle create and edit method.
     *
     * @param  int    $id
     * @return \Illuminate\Http\Response
     */
    protected function createEdit($id = 0)
    {
        $data = [
            'title'=>'Add Carousel',
            'form' => [
                'url' => route('admin.carousel.store'),
                'files' => true,
            ],
            'data' => [
                'title' => null,
                'image' => null,
                'description' => null
            ]
        ];

        if ($id > 0) {
            $data['title']='Edit Carousel';
            $data['form']['url'] = route('admin.carousel.update', $id);
            $data['form']['method'] = 'PATCH';
            $data['data'] = Carousel::findOrFail($id);
        }

        return view('backend.carousel.form', $data);
    }

    /**
     * Handle store and update method.
     * 
     * @param  App\Http\Requests\Backend\UserTrusteeRequest $request
     * @param  int                                          $id
     * @return \Illuminate\Http\Response
     */
    private function storeUpdate(Request $request, $id = 0)
    {
        $data = $request->except('_token', 'image');
        if ($request->hasFile('image')) {
            if ($image = $this->processImage($request)) {
                $data['image'] = $image;
            }
        }
        DB::beginTransaction();
        try {
            if ($id) {
                $carousel = Carousel::find($id);
                if (isset($data['image'])) {
                    $this->deleteimage($carousel->image);
                }

                $user = Carousel::find($id)->update($data);
            } else {
                $user = Carousel::create($data);
            }
            DB::commit();
            return redirect(route('admin.carousel'));
        } catch (Exception $e) {
            DB::rollback();
        }

        return redirect(route('admin.carousel'));
        
    }
    /**
     * Process image file request.
     * 
     * @param  \App\Http\Requests\Backend\UserTrusteeRequest $request
     * @return bool|string
     */
    private function processimage(Request $request)
    {
        $file = $request->file('image');

        if (! $file->isValid()) {
            return false;
        }

        $fileName = date('Y_m_d_His').'_'.$file->getClientOriginalName();

        // Move, move, move!!!
        $file->move(images_path('carousel'), $fileName);

        return $fileName;
    }

    /**
     * Process delete image.
     * 
     * @param  string $path
     * @return bool
     */
    private function deleteimage($path)
    {
        if (! $path) {
            return true;
        }

        $path = images_path('carousel/'.$path);

        if (! file_exists($path)) {
            return true;
        }

        if (! unlink($path)) {
            return false;
        }

        return true;
    }
}
