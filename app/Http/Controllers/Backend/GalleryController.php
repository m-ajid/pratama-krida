<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\ImageUpload;
use App\Models\UploadTemp;
use App\Models\Gallery;
use Sentinel;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $album=Gallery::album(true)->get();
        $album2=Gallery::album()->get();
        return view('backend.gallery.index',compact('album','album2'));
    }

    public function uploadImage(Request $request){
        $response=ImageUpload::upload($request->all());
        return $response;
    }

    public function create()
    {
        return $this->createEdit();
    }
    public function createAlbum()
    {
        return $this->createEdit(true);
    }

    public function album($album_name){
        $album=Gallery::imageByAlbum($album_name)->get();
        return view('backend.gallery.album',compact('album'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->storeUpdate($request);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return $this->createEdit($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->storeUpdate($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $response=ImageUpload::delete($id);
        return $response;
    }

    /**
     * Handle create and edit method.
     *
     * @param  int    $id
     * @return \Illuminate\Http\Response
     */
    protected function createEdit($is_album=null)
    {
        $data = [
            'title' => '',
            'form' => [
                'url' => route('admin.gallery.store'),
                'files' => true,
                'class'=>'dropzone',
            ]
        ];
        if ($is_album){
            return view('backend.gallery.form_album', compact('data'));
        }

        return view('backend.gallery.form', compact('data'));
    }

    /**
     * Handle store and update method.
     * 
     * @param  App\Http\Requests\Backend\UserTrusteeRequest $request
     * @param  int                                          $id
     * @return \Illuminate\Http\Response
     */
    private function storeUpdate(Request $request, $id = 0)
    {
        $data = $request->except('_token','imageUpload','label');
        $data['label']='Other';
        if (!$request->label==''){
            $data['label']=$request->label;
        }

        if ($request->hasFile('imageUpload')) {
            if ($image = $this->multipleUpload($request,$data['label'])) {
                $data['image'] = $image;
            }
        }
        Gallery::create($data);
        return redirect(route('admin.gallery'));
    }
    private function multipleUpload(Request $request,$album_name=null)
    {
        $file = $request->file('imageUpload');
        foreach ($file as $key => $value) {
            $fileName = date('Y_m_d_His').'_'.$value->getClientOriginalName();
            $image[] = $fileName;
            $value->move(gallery_path($album_name), $fileName);
        }
        
        return implode(',', $image);
    }

}
