<?php

namespace App\Http\Controllers\Backend;

// use Illuminate\Http\Request;
// use App\Http\Controllers\Controller;
use Mail;
use Event;
use Reminder;
use Sentinel;
use App\Http\Controllers\Controller;
use App\Events\Backend\ResetPasswordEvent;
use Illuminate\Http\Request as BaseRequest;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use App\Http\Requests\Backend\WebLoginRequest as Request;
class AuthController extends Controller
{
    /**
     * Create a new Backend\AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('sentinel_guest', ['except' => 'getLogout']);
    }

    /**
     * Login page.
     * 
     * @return \Illuminate\Http\Response
     */
    public function getLogin()
    {
        $form = [
            'url' => action('Backend\AuthController@postLogin'),
            'autocomplete' => 'off',
        ];

        return view('backend.login', compact('form'));
    }

    /**
     * Handle login request.
     *
     * @param  \App\Http\Requests\Auth\WebLoginRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function postLogin(Request $request)
    {
        $backToLogin = redirect()->action('Backend\AuthController@getLogin')->withInput();
        $findUser = Sentinel::findByCredentials(['login' => $request->input('email')]);

        // If we can not find user based on email...
        if (! $findUser) {
            flash()->error('Wrong email or username!');
            return $backToLogin;
        }

        try {
            $remember = (bool) $request->input('remember_me');
            // If password is incorrect...
            if (! Sentinel::authenticate($request->all(), $remember)) {
                flash()->error('Password is incorrect!');
                return $backToLogin;
            }

            flash()->success('Login success!');

            return redirect()->intended('admin/dashboard');
        } catch (ThrottlingException $e) {
            flash()->error('Too many attempts!');
        } catch (NotActivatedException $e) {
            flash()->error('Please activate your account before trying to log in.');
        }
        return $backToLogin;
    }

    /**
     * Handle logout request.
     * 
     * @return \Illuminate\Http\Response
     */
    public function getLogout()
    {
        Sentinel::logout();

        return redirect()->action('Backend\AuthController@getLogin');
    }

    /**
     * Reset password page.
     * 
     * @return \Illuminate\Http\Response
     */
    public function getResetPassword()
    {
        $form = [
            'url' => action('Backend\AuthController@postResetPassword'),
            'autocomplete' => 'off',
        ];

        return view('backend.auth.reset-password', compact('form'));
    }

    /**
     * Process reset password.
     * 
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function postResetPassword(BaseRequest $request)
    {
        $this->validate($request, ['email' => 'required|email']);

        $return = redirect()->action('Backend\AuthController@getResetPassword')->withInput();
        $findUser = Sentinel::findByCredentials(['login' => $request->input('email')]);

        // If we can not find user based on email...
        if (! $findUser) {
            flash()->error('Email is not registered.');

            return $return;
        }

        ($reminder = Reminder::exists($findUser)) || ($reminder = Reminder::create($findUser));

        Event::fire(new ResetPasswordEvent($findUser, $reminder));

        flash()->success('Check your inbox to reset password!');

        return redirect()->action('Backend\AuthController@getLogin');
    }

    /**
     * Change password page.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function getChangePassword(BaseRequest $request)
    {
        $user = Sentinel::findById($request->input('id'));

        if (! Reminder::exists($user, $request->input('code'))) {
            flash()->error('You have no right to change password!');

            return redirect()->action('Backend\AuthController@getLogin');
        }

        $data = [
            'form' => [
                'url' => action('Backend\AuthController@postChangePassword'),
                'autocomplete' => 'off',
            ],
            'data' => $request->all(), 
        ];

        return view('backend.auth.change-password', $data);
    }

    /**
     * Process change password.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function postChangePassword(BaseRequest $request)
    {
        $this->validate($request, [
            'password' => 'required|confirmed',
            'password_confirmation' => 'required',
        ]);

        $user = Sentinel::findById($request->input('id'));
        Reminder::complete($user, $request->input('code'), $request->input('password'));

        flash()->success('Password successfully changed!');

        return redirect()->action('Backend\AuthController@getLogin');
    }
}
