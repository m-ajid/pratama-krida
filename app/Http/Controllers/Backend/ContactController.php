<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ContactUs;
use DB;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id=null)
    {
        return view('backend.setting.contact');
    }

    public function create()
    {
        return $this->createEdit();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->storeUpdate($request);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $id=ContactUs::first()->id;
        return $this->createEdit($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->storeUpdate($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->transaction(function ($model) use ($id) {
            $user = Sentinel::findById($id);
            $this->deleteAvatar($user->avatar);
            $user->delete();
        }, true);
    }

    /**
     * Handle create and edit method.
     *
     * @param  int    $id
     * @return \Illuminate\Http\Response
     */
    protected function createEdit($id = 0)
    {
        $data = [
            'title' => 'Manage Contact',
            'form' => [
                'url' => route('admin.settings.contact.update', $id)
            ],
            'contact' => [
                'company'=>null,
                'address'=>null,
                'phone'=>null,
                'mobile'=>null,
                'email'=>null,
                'map'=>null,
                'social_media'=>null,
                'image' => null
            ]
        ];

        if ($id > 0) {
            $data['form']['url'] = route('admin.settings.contact.update', $id);
            $data['form']['method'] = 'PATCH';
            $data['contact'] = ContactUs::findOrFail($id);
        }

        return view('backend.setting.contact', $data);
    }

    /**
     * Handle store and update method.
     * 
     * @param  App\Http\Requests\Backend\UserTrusteeRequest $request
     * @param  int                                          $id
     * @return \Illuminate\Http\Response
     */
    private function storeUpdate(Request $request, $id = 0)
    {
        $data = $request->except('_token', 'image');
        if ($request->hasFile('image')) {
            if ($image = $this->processImage($request)) {
                $data['image'] = $image;
            }
        }
        DB::beginTransaction();
        try {
            if ($id) {
                $service = ContactUs::find($id);
                if (isset($data['image'])) {
                    $this->deleteimage($service->image);
                }

                $user = ContactUs::find($id)->update($data);
            } else {
                $user = ContactUs::create($data);
            }
            DB::commit();
            return redirect(route('admin.settings.contact.edit'));
        } catch (Exception $e) {
            DB::rollback();
        }

        return redirect(route('admin.settings.contact.edit'));
    }

    /**
     * Process image file request.
     * 
     * @param  \App\Http\Requests\Backend\UserTrusteeRequest $request
     * @return bool|string
     */
    private function processimage(Request $request)
    {
        $file = $request->file('image');

        if (! $file->isValid()) {
            return false;
        }

        $fileName = date('Y_m_d_His').'_'.$file->getClientOriginalName();

        // Move, move, move!!!
        $file->move(images_path(), $fileName);

        return $fileName;
    }

    /**
     * Process delete image.
     * 
     * @param  string $path
     * @return bool
     */
    private function deleteimage($path)
    {
        if (! $path) {
            return true;
        }

        $path = images_path($path);

        if (! file_exists($path)) {
            return true;
        }

        if (! unlink($path)) {
            return false;
        }

        return true;
    }
}
