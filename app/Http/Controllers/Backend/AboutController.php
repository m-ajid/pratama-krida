<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\AboutUs;
use DB;

class AboutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $about=AboutUs::first();
        $data = [
            'title' => 'About Us',
            'form' => [
                'url' => route('admin.about.update',$about->id),
                'method'=>'PATCH',
                'files' => true,
            ],
            'about' => $about
        ];
        
        // return view('vendor.laravel-filemanager.demo');
        return view('backend.setting.about',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->storeUpdate($request, $id);
    }

    /**
     * Handle store and update method.
     * 
     * @param  App\Http\Requests\Backend\UserTrusteeRequest $request
     * @param  int                                          $id
     * @return \Illuminate\Http\Response
     */
    private function storeUpdate(Request $request, $id = 1)
    {
        $data = $request->except('_token','image');
        if ($request->hasFile('image')) {
            if ($image = $this->processAvatar($request)) {
                $data['image'] = $image;
            }
        }

        DB::beginTransaction();
        try {
            AboutUs::find($id)->update($data);
            DB::commit();
            flash()->success(trans('general.save_success'));
        } catch (Exception $e) {
            DB::rollback();
            flash()->error(trans('general.save_success'));
        }
        return back();
    }

    /**
     * Process avatar file request.
     * 
     * @param  \App\Http\Requests\Backend\UserTrusteeRequest $request
     * @return bool|string
     */
    private function processAvatar(Request $request)
    {
        $file = $request->file('image');

        if (! $file->isValid()) {
            return false;
        }

        $fileName = date('Y_m_d_His').'_'.$file->getClientOriginalName();

        // Move, move, move!!!
        $file->move(images_path(), $fileName);

        return $fileName;
    }

    /**
     * Process delete avatar.
     * 
     * @param  string $path
     * @return bool
     */
    private function deleteAvatar($path)
    {
        if (! $path) {
            return true;
        }

        $path = images_path($path);

        if (! file_exists($path)) {
            return true;
        }

        if (! unlink($path)) {
            return false;
        }

        return true;
    }
}
