<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DataTables\MessageDataTables as Datatables;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Datatables $dataTable)
    {
        return $dataTable->render('backend.message.index');
    }

    public function create()
    {
        return $this->createEdit();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->storeUpdate($request);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return $this->createEdit($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->storeUpdate($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->transaction(function ($model) use ($id) {
            $user = Sentinel::findById($id);
            $this->deleteAvatar($user->avatar);
            $user->delete();
        }, true);
    }

    /**
     * Datatables for User Trustee Management.
     * 
     * @return \Illuminate\Http\Response
     */
    public function datatables() 
    {
        return datatables(User::datatables(true))
                ->addColumn('role', function ($user) {
                    return User::findOrFail($user->id)->roles[0]->name;
                })
                ->addColumn('action', function ($user) {
                    if (Sentinel::findById($user->id)->roles[0]->is_super_admin) {
                        return;
                    }

                    $url = action('Backend\UserTrustee\UserController@edit', $user->id);

                    return '<a href="'.$url.'" class="btn btn-warning" title="Edit"><i class="fa fa-pencil-square-o fa-fw"></i></a>&nbsp;<a href="#" class="btn btn-danger" title="Delete" data-id="'.$user->id.'" data-button="delete"><i class="fa fa-trash-o fa-fw"></i></a>';
                })
                ->editColumn('last_login', function ($user) {
                    if (is_null($user->last_login)) {
                        return '--';
                    }

                    return mybal_datetime($user->last_login);
                })
                ->make(true);
    }

    /**
     * Handle create and edit method.
     *
     * @param  int    $id
     * @return \Illuminate\Http\Response
     */
    protected function createEdit($id = 0)
    {
        $data = [
            'title' => ucfirst(mybal_form_title($id)),
            'form' => [
                'url' => action('Backend\UserTrustee\UserController@store'),
                'files' => true,
            ],
            'user' => [
                'email' => null,
                'first_name' => null,
                'last_name' => null,
                'avatar' => null,
                'role' => null,
            ],
            'dropdown' => Role::dropdown(),
        ];

        if ($id > 0) {
            $data['form']['url'] = action('Backend\UserTrustee\UserController@update', $id);
            $data['form']['method'] = 'PUT';
            $data['user'] = User::findOrFail($id);
            $data['user']['role'] = $data['user']->roles[0]->id;
        }

        return view('backend.user-trustee.user.form', $data);
    }

    /**
     * Handle store and update method.
     * 
     * @param  App\Http\Requests\Backend\UserTrusteeRequest $request
     * @param  int                                          $id
     * @return \Illuminate\Http\Response
     */
    private function storeUpdate(Request $request, $id = 0)
    {
        $data = $request->except('_token', 'avatar', 'role');
        if ($request->hasFile('avatar')) {
            if ($avatar = $this->processAvatar($request)) {
                $data['avatar'] = $avatar;
            }
        }

        if (! $id) {
            $data['password'] = '12345678';
            $data['is_admin'] = true;
        }

        // Saving to database...
        return $this->transaction(function ($model) use ($id, $request, $data) {
            if ($id) {
                $user = Sentinel::findById($id);
                if (isset($data['avatar'])) {
                    $this->deleteAvatar($user->avatar);
                }

                $role = Sentinel::findRoleById($user->roles[0]->id);
                $role->users()->detach($user);

                $user = Sentinel::update($user, $data);
            } else {
                $user = Sentinel::registerAndActivate($data);
            }

            $role = Sentinel::findRoleById($request->input('role'));
            $role->users()->attach($user);
        });
    }

}
