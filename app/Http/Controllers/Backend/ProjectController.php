<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DataTables\ProjectDataTables as Datatables;
use App\Models\Project;
use DB;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Datatables $dataTable)
    {
        return $dataTable->render('backend.project.index');
    }

    public function create()
    {
        return $this->createEdit();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->storeUpdate($request);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return $this->createEdit($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->storeUpdate($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $_data = Project::find($id);
            $this->deleteimage($_data->image);
            $_data->delete();
            DB::commit();
            return redirect(route('admin.project'));
        } catch (Exception $e) {
            DB::rollback();
        }

        return redirect(route('admin.project'));
    }

    /**
     * Handle create and edit method.
     *
     * @param  int    $id
     * @return \Illuminate\Http\Response
     */
    protected function createEdit($id = 0)
    {
        $data = [
            'title'=>'Add Project',
            'form' => [
                'url' => route('admin.project.store'),
                'files' => true,
            ],
            'data' => [
                'title' => null,
                'image' => null,
                'description' => null,
                'short_description'=>null
            ]
        ];

        if ($id > 0) {
            $data['title']='Edit Service';
            $data['form']['url'] = route('admin.project.update', $id);
            $data['form']['method'] = 'PATCH';
            $data['data'] = Project::findOrFail($id);
        }

        return view('backend.project.form', $data);
    }

    /**
     * Handle store and update method.
     * 
     * @param  App\Http\Requests\Backend\UserTrusteeRequest $request
     * @param  int                                          $id
     * @return \Illuminate\Http\Response
     */
    private function storeUpdate(Request $request, $id = 0)
    {
        $data = $request->except('_token', 'image');
        if ($request->hasFile('image')) {
            if ($image = $this->processImage($request)) {
                $data['image'] = $image;
            }
        }
        DB::beginTransaction();
        try {
            if ($id) {
                $_data = Project::find($id);
                if (isset($data['image'])) {
                    $this->deleteimage($_data->image);
                }

                Project::find($id)->update($data);
            } else {
                Project::create($data);
            }
            DB::commit();
            return redirect(route('admin.project'));
        } catch (Exception $e) {
            DB::rollback();
        }

        return redirect(route('admin.project'));
    }
    /**
     * Process image file request.
     * 
     * @param  \App\Http\Requests\Backend\UserTrusteeRequest $request
     * @return bool|string
     */
    private function processimage(Request $request)
    {
        $file = $request->file('image');

        if (! $file->isValid()) {
            return false;
        }

        $fileName = date('Y_m_d_His').'_'.$file->getClientOriginalName();

        // Move, move, move!!!
        $file->move(images_path('project'), $fileName);

        return $fileName;
    }

    /**
     * Process delete image.
     * 
     * @param  string $path
     * @return bool
     */
    private function deleteimage($path)
    {
        if (! $path) {
            return true;
        }

        $path = images_path('project/'.$path);

        if (! file_exists($path)) {
            return true;
        }

        if (! unlink($path)) {
            return false;
        }

        return true;
    }
}
