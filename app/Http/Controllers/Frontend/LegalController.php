<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LegalController extends Controller
{
    public function index (){
        $filename = 'Legalitas.pdf';
        $path = asset('storage/files/Legalitas.pdf');
        return  view('frontend.legalitas');
        return \Response::make(file_get_contents($path), 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="'.$filename.'"'
         ]);
    }
}
