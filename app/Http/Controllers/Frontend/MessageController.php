<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Message;
use DB;
class MessageController extends Controller
{
    public function store(Request $request){
        DB::beginTransaction();
        try {
            $_data=$request->except('_token');
            Message::create($_data);
            DB::commit();
            return redirect()->back()->withSuccess('Message was Sent');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->withError('Message can not Sent');
        }
    }
}
