<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Service;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hasService=Service::select(\DB::raw('DISTINCT(category)'))->get();
        foreach ($hasService as $key => $value) {
            $_data[]=['category'=>$value->category,'data'=>Service::where('category','=',$value->category)->get()];
        }
        $hasService=$_data;
        return view('frontend.service',compact('hasService'));
    }
}
