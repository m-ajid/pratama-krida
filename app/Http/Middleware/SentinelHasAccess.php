<?php

namespace App\Http\Middleware;

use Closure;
use HasAccess;
class SentinelHasAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $permission)
    {
        if (! HasAccess::hasAccess($permission)) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 403);
            }

            return response()->view('backend.unauthorized');
        }
        return $next($request);
    }
}
