<?php

namespace App\Http\Requests\Backend\UserTrustee;

use App\Http\Requests\Request;

class UserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'avatar' => 'image',
            'email' => 'required|email',
            'first_name' => 'required',
            'last_name' => 'required',
        ];
    }
}
