<?php

namespace App\DataTables;

use App\Models\Service;
use Yajra\Datatables\Services\DataTable;

class ServiceDataTables extends DataTable
{
    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('option', function($data){
                return '
                <a href="'.route('admin.service.edit', $data->id).'" class="btn btn-warning" title="Edit"><i class="fa fa-pencil-square-o fa-fw"></i></a>&nbsp;
                <a href="#" class="btn btn-danger btn-delete" title="Delete" data-id="'.$data->id.'" data-button="delete"><i class="fa fa-trash-o fa-fw"></i></a>
                ';
            })
            ->addColumn('images', function($data){
                $image='<img src="'.link_to_images('noimage.gif') .'" class="img-responsive img-thumbnail img-rounded">';
                if ($data->image){
                    $image='<img src="'.asset('storage/images/service').'/'.$data->image.'" class="img-responsive img-thumbnail img-rounded">';
                }
                return $image;
            })
            ->rawColumns(['option','images','description'])
            ->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query =Service::select('id','title','image','description');
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->ajax('')
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'title',
            'images',
            'description',
            'option'
        ];
    }
    public function getBuilderParameters()
    {
        return ['dom' => 'Bfrtip',
            "lengthMenu"=> [ [10, 25, 50, 100,250,500,1000 -1], [10, 25, 50, 100,250,500,1000] ],
            'buttons' => [
                'pageLength',
                ['extend'=>'reload','text'=>'<i class="fa fa-refresh"></i> &nbsp; Reload', 'titleAttr'=>'Reload', 'className'=>'btn-info'],
                ['extend'=>'collection','autoClose'=> true,
                    'text'=>'<i class="fa fa-download"></i> &nbsp; Export To &nbsp;<i class="fa fa-caret-down"></i>',
                    'buttons'=>[
                            ['extend'=>'excel','text'=>'<i class="fa fa-file-excel-o"></i> &nbsp; Export to Excel', 'titleAttr'=>'Export to Excel', 'className'=>'btn btn-info'],
                            ['extend'=>'csv','text'=>'<i class="fa fa-file-text-o"></i> &nbsp; Export to CSV', 'titleAttr'=>'Export to CSV', 'className'=>'btn-info'],
                            ['extend'=>'pdf','text'=>'<i class="fa fa-file-pdf-o"></i> &nbsp; Export to PDF', 'titleAttr'=>'Export to PDF', 'className'=>'btn-info'],
                    ]
                ],
                ['extend'=>'print','text'=>'<i class="fa fa-print"></i> &nbsp; Print', 'titleAttr'=>'Print', 'className'=>'btn-info','autoPrint'=> false,
                    'exportOptions'=> [
                        'footer'=> false,
                    ],
                    'customize'=> "function (win) {
                        $(win.document.body).find('table').addClass('display').css('font-size', '9px');
                        $(win.document.body).find('tr:nth-child(odd) td').each(function(index){
                            $(this).css('background-color','#D0D0D0');
                        });
                        $(win.document.body).find('h1').css('text-align','center');
                    }"
                ],
            ],
            ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'servicedatatables_' . time();
    }
}
