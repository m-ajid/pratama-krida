<?php

namespace App\DataTables;

use App\Models\Message;
use Yajra\Datatables\Services\DataTable;

class MessageDataTables extends DataTable
{
    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = Message::select('name','email','message','is_read');
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->ajax('')
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'name',
            'email',
            'message',
            'is_read'
        ];
    }

    public function getBuilderParameters()
    {
        return ['dom' => 'Bfrtip',
            "lengthMenu"=> [ [10, 25, 50, 100,250,500,1000 -1], [10, 25, 50, 100,250,500,1000] ],
            'buttons' => [
                'pageLength',
                ['extend'=>'reload','text'=>'<i class="fa fa-refresh"></i> &nbsp; Reload', 'titleAttr'=>'Reload', 'className'=>'btn-info'],
                ['extend'=>'collection','autoClose'=> true,
                    'text'=>'<i class="fa fa-download"></i> &nbsp; Export To &nbsp;<i class="fa fa-caret-down"></i>',
                    'buttons'=>[
                            ['extend'=>'excel','text'=>'<i class="fa fa-file-excel-o"></i> &nbsp; Export to Excel', 'titleAttr'=>'Export to Excel', 'className'=>'btn btn-info'],
                            ['extend'=>'csv','text'=>'<i class="fa fa-file-text-o"></i> &nbsp; Export to CSV', 'titleAttr'=>'Export to CSV', 'className'=>'btn-info'],
                            ['extend'=>'pdf','text'=>'<i class="fa fa-file-pdf-o"></i> &nbsp; Export to PDF', 'titleAttr'=>'Export to PDF', 'className'=>'btn-info'],
                    ]
                ],
                ['extend'=>'print','text'=>'<i class="fa fa-print"></i> &nbsp; Print', 'titleAttr'=>'Print', 'className'=>'btn-info','autoPrint'=> false,
                    'exportOptions'=> [
                        'footer'=> false,
                    ],
                    'customize'=> "function (win) {
                        $(win.document.body).find('table').addClass('display').css('font-size', '9px');
                        $(win.document.body).find('tr:nth-child(odd) td').each(function(index){
                            $(this).css('background-color','#D0D0D0');
                        });
                        $(win.document.body).find('h1').css('text-align','center');
                    }"
                ],
            ]
            ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'messagedatatables_' . time();
    }
}
