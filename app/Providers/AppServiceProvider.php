<?php

namespace App\Providers;
use Blade;
use Sentinel;
use Queue;
use App\Models\Menu;
use App\Models\ContactUs;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\ServiceProvider;
use  Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        
        $this->bootBladeCustomDirectives();
        $this->bootMenuViewComposer();
        // // $this->bootSkinComposer();
        $this->bootMenuViewFooter();
        Schema::defaultStringLength(255);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap our blade custom directives.
     * 
     * @return void 
     */
    private function bootBladeCustomDirectives()
    {
        Blade::directive('hasaccess', function ($expression) {
            if (Sentinel::check()) {
                return "<?php if (HasAccess::hasAccess{$expression}): ?>";
            }

            return;
        });

        Blade::directive('endhasaccess', function ($expression) {
            if (Sentinel::check()) {
                return "<?php endif; ?>";
            }

            return;
        });
    }

    /**
     * Bootstrap our menus.
     * 
     * @return void
     */
    private function bootMenuViewComposer()
    {
        view()->composer('backend.template.partial.sidebar', function ($view) {
            $index = 0;
            $menus = Menu::where('parent', null)->get()->toArray();

            foreach ($menus as $menu) {
                if ((bool) $menu['is_parent']) {
                    if ($child = Menu::where('parent', $menu['id'])->get()->toArray()) {
                        foreach ($child as $value) {
                            $menus[$index]['child'][] = $value;
                            $menus[$index]['child_permissions'][] = $value['name'];
                        }
                    }
                }

                $index++;
            }

            $view->withMenus($menus);
        });
    }
    
    private function bootMenuViewFooter()
    {   
        $footer=[];
        if (Schema::hasTable('contact_uses')){
            $footer=$footer=ContactUs::first()->toArray();
        }
        view()->share('hasFooter', $footer);
    }
}
