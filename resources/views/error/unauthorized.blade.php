@extends('error.template')

@section('title', 'Unauthorized')

@section('content')
    <h1 align="center">You have no rights to access this page!</h1>
@endsection