@extends('backend.template.master') 
@section('title','About')
@section('style')
    {!! Html::style('bower_components/AdminLTE/plugins/jvectormap/jquery-jvectormap-1.2.2.css') !!}
    <style type="text/css">
        .remove-img-preview{
            background-image: url('../../storage/images/icon/remove-red.png') ;
            background-position: center;
            background-repeat: no-repeat;
            background-size: 100%;
            float: right;
            right: 15px;
            top: 0px;
            position: absolute;
            z-index: 999;
            width:35px;
            height: 35px;
        }
        .img-preview{
            margin:15px;
            height:250px;
        }
    </style>
@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li></a href="{{ route('admin.gallery') }}"><i class="fa fa-picture-o"></i> Gallery</a></li>
        <li class="active"> Upload</li>
    </ol>
@endsection
@section('page-header', 'Gallery Upload')
@section('content') 


<!-- /.row -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Monthly Recap Report</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            {!! Form::model('upload',['route'=>'admin.gallery.store','files'=>true]) !!}
                <div class="box-body">
                    <div class="form-group has-error">
                      <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> Title</label>
                      <input type="text" class="form-control" name="title" id="inputError">
                    </div>
                    <div class="form-group has-error">
                      <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> Description</label>
                      <textarea type="text" class="form-control" name="description" id="inputError" rows="5"></textarea> 
                    </div>
                    <div class="form-group has-error">
                      <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> Image</label>
                      <input type="file" name="imageUpload[]" class="form-control" id="imageUpload" onchange="makeFileList()" multiple>
                      <span class="help-block">Help block with error</span>
                    </div>
                    <div class="form-group has-error">
                      <ul id="fileList"><li>No Files Selected</li></ul>
                      <div id="image_preview" class='row'></div>
                    </div>
                </div>
                <div class="box-footer">
                    {!! link_to_action('Backend\GalleryController@index', 'Back', [], ['class' => 'btn btn-default']).' '.Form::submit('Save', ['class' => 'btn btn-primary pull-right', 'title' => 'Save']) !!}
                </div>
            {!! Form::close() !!}
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>

<!-- /.row -->
@endsection
@section('scripts') 
    
    {!! Html::script('bower_components/AdminLTE/plugins/sparkline/jquery.sparkline.min.js') !!}
    {!! Html::script('bower_components/AdminLTE/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') !!}
    {!! Html::script('bower_components/AdminLTE/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') !!}
    {!! Html::script('bower_components/AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js') !!}
    {!! Html::script('bower_components/dropzone/dist/dropzone.js') !!}
    
    <script>
        function makeFileList(event){
            var input = document.getElementById('imageUpload');
            var imagePreview = document.getElementById('image_preview');
            if (input.files && input.files.length) {
                for (var x = 0; x < input.files.length; x++) {
                    var reader = new FileReader();
                    reader.onload = function(event) {
                        $(imagePreview).append('<div class="col-md-4 col-xs-6 col-preview"><div class="remove-img-preview" onclick="removePreview()"></div><div class="img-preview"><img class="img-responsive img-thumbnail img-rounded" src="'+event.target.result+'"></div></div>');
                    }
                    reader.readAsDataURL(input.files[x]);
                }
            }
        }
    </script>
@endsection