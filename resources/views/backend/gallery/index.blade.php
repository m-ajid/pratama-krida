@extends('backend.template.master') 
@section('title','About')
@section('style')
    {!! Html::style('bower_components/AdminLTE/plugins/jvectormap/jquery-jvectormap-1.2.2.css') !!}
    <style type="text/css">
        .remove-img-preview{
            background-image: url('../../storage/images/icon/remove-red.png') ;
            background-position: center;
            background-repeat: no-repeat;
            background-size: 100%;
            float: right;
            right: 15px;
            top: 0px;
            position: absolute;
            z-index: 999;
            width:35px;
            height: 35px;
        }
        .img-preview{
            margin:15px;
            height:250px;
        }

        .folder-album{
            position: relative;
            margin: auto;
        }
        .button-group{
            margin-top:5px;
            margin-bottom: 25px;
        }
    </style>
@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active"></a href="#"><i class="fa fa-picture-o"></i> Gallery</a></li>
    </ol>
@endsection
@section('page-header', 'Gallery')
@section('content') 


<!-- /.row -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Photo Album</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="button-group">
                    <a class="btn btn-default" href="{{ route('admin.gallery.create') }}">Upload Image <i class="fa fa-camera"></i></a>
                    <a class="btn btn-default" href="{{ route('admin.gallery.create-album') }}">Create Album <i class="fa fa-camera"></i></a>
                </div>
                <div class="row">
                    @foreach($album as $albums)
                    <div class="album-group col-md-3 col-sm-3 col-lg-3 col-md-4">
                        <a class="text-center" href="{{ route('admin.gallery.album',$albums['label']) }}">
                            <img class="img-responsive folder-album" src="{{ asset('storage/images/icon/folder_default.png') }}">
                        </a>
                        <h5 class="text-center"><a class="text-center" href="{{ route('admin.gallery.album',$albums['label']) }}">{{ $albums['label'] }}</a></h5>
                    </div> 
                   @endforeach

                   @foreach($album2 as $album)
                    <?php $images=explode(',',$album['image']);?>
                        @foreach($images as $image)
                            <div class="album-group col-md-3 col-sm-3 col-lg-3 col-md-4">
                                <a class="text-center" href="{{ route('admin.gallery.album',$album['label']) }}">
                                    <img src="{{ link_to_gallery('Other',$image)}}" class="img-responsive img-thumbnail img-rounded">
                                    @if (substr($image,-4)=='.mp4')
                                  <video width="100%" controls>
                                    <source src="{{ link_to_gallery('Other',$image)}}" type="video/mp4">
                                    <source src="{{ link_to_gallery('Other',str_replace('.mp4','.ogg',$image))}}" type="video/ogg">
                                      Your browser does not support HTML5 video.
                                  </video>
                                @else
                                  <img src="{{ link_to_gallery('Other',$image)}}" class="img-responsive img-thumbnail img-rounded">
                                @endif
                                </a>
                                <h5 class="text-center"><a class="text-center" href="{{ route('admin.gallery.album',$album['title']) }}">{{ $album['title'] }}</a></h5>
                            </div> 
                        @endforeach
                   @endforeach
                </div>
            </div>
            <div class="box-footer">
                <a href="{{ route('admin.dashboard') }}" class="btn btn-default">Back</a>
            </div>
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->
@endsection
@section('scripts') 
    
    {!! Html::script('bower_components/AdminLTE/plugins/sparkline/jquery.sparkline.min.js') !!}
    {!! Html::script('bower_components/AdminLTE/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') !!}
    {!! Html::script('bower_components/AdminLTE/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') !!}
    {!! Html::script('bower_components/AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js') !!}
    <script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
    <script>
        $(function () {
        CKEDITOR.replace('editor1');
      });
    </script>
@endsection