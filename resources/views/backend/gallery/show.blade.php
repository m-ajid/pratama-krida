@extends('backend.template.master') 
@section('title','About')
@section('style')
    {!! Html::style('bower_components/AdminLTE/plugins/jvectormap/jquery-jvectormap-1.2.2.css') !!}
    <style type="text/css">
        .remove-img-preview{
            background-image: url('../../storage/images/icon/remove-red.png') ;
            background-position: center;
            background-repeat: no-repeat;
            background-size: 100%;
            float: right;
            right: 15px;
            top: 0px;
            position: absolute;
            z-index: 999;
            width:35px;
            height: 35px;
        }
        .img-preview{
            margin:15px;
            height:250px;
        }

        .folder-album{
            position: relative;
            margin: auto;
        }
    </style>
@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li></a href="#"><i class="fa fa-gears"></i> Settings</a></li>
        <li class="active"> About</li>
    </ol>
@endsection
@section('content') 


<!-- /.row -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Monthly Recap Report</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-default">Cancel</button>
            </div>
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->
@endsection
@section('scripts') 
    
    {!! Html::script('bower_components/AdminLTE/plugins/sparkline/jquery.sparkline.min.js') !!}
    {!! Html::script('bower_components/AdminLTE/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') !!}
    {!! Html::script('bower_components/AdminLTE/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') !!}
    {!! Html::script('bower_components/AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js') !!}
    <script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
    <script>
        $(function () {
        CKEDITOR.replace('editor1');
      });
    </script>
@endsection