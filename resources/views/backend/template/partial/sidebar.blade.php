<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
<!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
    <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ link_to_avatar(user_info('avatar')) }}" alt="{{ user_info('full_name') }}" class="img-circle">
            </div>
            <div class="pull-left info">
                <p>{{ user_info('full_name') }}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form (Optional) -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
                <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            @foreach ($menus as $menu)
                @if ($menu['is_parent'])
                    @hasaccess((isset($menu['child_permissions']) ? $menu['child_permissions'] : []))
                        <li class="treeview{!! (Request::is($menu['pattern'].'/*')) ? ' active' : '' !!}">
                            <a href="#" title="{{ $menu['display_name'] }}">
                                <i class="fa fa-{{ $menu['icon'] }}"></i> <span>{{ $menu['display_name'] }}</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            @if (isset($menu['child']))
                                <ul class="treeview-menu">
                                    @foreach($menu['child'] as $child)
                                        @hasaccess((isset($child['name']) ? $child['name'] : []))
                                        
                                            <li{!! (url($child['href']) == Request::url() OR Request::is($child['href'].'/*')) ? ' class="active"' : '' !!}>
                                                <a href="{!! url($child['href']) !!}" title="{{ $child['display_name'] }}">
                                                    <i class="fa fa-{{ $child['icon'] }}"></i> {{ $child['display_name'] }}
                                                </a>
                                            </li>
                                        @endhasaccess
                                    @endforeach
                                </ul>
                            @endif
                        </li>
                    @endhasaccess
                @else
                    @hasaccess((isset($menu['name']) ? $menu['name'] : []))
                        <li{!! (url($menu['href']) == Request::url() OR Request::is($menu['href'].'/*')) ? ' class="active"' : '' !!}>
                            <a href="{!! url($menu['href']) !!}" title="{!! $menu['display_name'] !!}">
                                <i class="fa fa-{{ $menu['icon'] }}"></i> <span>{{ $menu['display_name'] }}</span>
                            </a>
                        </li>
                    @endhasaccess
                @endif
            @endforeach
        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>