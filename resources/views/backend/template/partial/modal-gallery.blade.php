@foreach($hasGallery as $key => $gallery)
      <?php $images=explode(',',$gallery['image']);?>
      @foreach($images as $key => $image)
        <div class="portfolio-modal modal fade" id="{{$gallery['title']}}{{ $key+1}}" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
              <div class="lr">
                <div class="rl"> </div>
              </div>
            </div>
            <div class="container">
              <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                  <div class="modal-body"> 
                    <!-- Project Details Go Here -->
                    <h2>{{ $gallery['title']}}</h2>
                    <img class="img-responsive img-centered" src="{{ link_to_gallery($gallery['label'],$image)}}" alt="">
                    <p>{{ $gallery['description']}}</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      @endforeach
    @endforeach