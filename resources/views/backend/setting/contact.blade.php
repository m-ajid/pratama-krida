@extends('backend.template.master') 
@section('title','Contact')
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li></a href="#"><i class="fa fa-gears"></i> Settings</a></li>
        <li class="active"> Contact</li>
    </ol>
@endsection
@section('style')
    {!! Html::style('bower_components/AdminLTE/plugins/jvectormap/jquery-jvectormap-1.2.2.css') !!}
@endsection

@section('content') 

<!-- /.row -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Edit Contact</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <div class="btn-group">
                        <button class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown"><i class="fa fa-wrench"></i></button>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="#">Action</a>
                            </li>
                            <li>
                                <a href="#">Another action</a>
                            </li>
                            <li>
                                <a href="#">Something else here</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">Separated link</a>
                            </li>
                        </ul>
                    </div>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            {!! Form::model($contact, $form) !!}
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div>
                                <h3>General</h3>
                            </div>
                            <div class="form-group">
                                <label>Company Name</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-pencil"></i>
                                    </div>
                                    {!! Form::text('company',null,['class'=>'form-control']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Address</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-address"></i>
                                    </div>
                                    {!! Form::textarea('address',null,['class'=>'form-control','rows'=>3]) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="inputSuccess"> Phone Number</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-phone"></i>
                                    </div>
                                    {!! Form::text('phone',null,['class'=>'form-control']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                              <label class="control-label" for="inputSuccess"> Mobile Number</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-mobile"></i>
                                    </div>
                                    {!! Form::text('mobile',null,['class'=>'form-control']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                              <label class="control-label" for="inputSuccess"> E-Mail</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-envelope-o"></i>
                                    </div>
                                    {!! Form::text('email',null,['class'=>'form-control']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                              <label class="control-label" for="inputSuccess"> Map</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-map"></i>
                                    </div>
                                    {!! Form::textarea('map',null,['class'=>'form-control','rows'=>3]) !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div>
                                <h3>Social Media</h3>
                            </div>
                            <div class="form-group">
                                <label>Facebook</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-pencil"></i>
                                    </div>
                                    {!! Form::text('facebook',null,['class'=>'form-control']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Twitter</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-pencil"></i>
                                    </div>
                                    {!! Form::text('twitter',null,['class'=>'form-control']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Instagram</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-pencil"></i>
                                    </div>
                                    {!! Form::text('instagram',null,['class'=>'form-control']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Google Plus</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-pencil"></i>
                                    </div>
                                    {!! Form::text('gplus',null,['class'=>'form-control']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Youtube</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-pencil"></i>
                                    </div>
                                    {!! Form::text('youtube',null,['class'=>'form-control']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Linked In</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-pencil"></i>
                                    </div>
                                    {!! Form::text('linked_in',null,['class'=>'form-control']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    {!! link_to_action('Backend\DashboardController@index', 'Back', [], ['class' => 'btn btn-default']).' '.Form::submit('Save', ['class' => 'btn btn-primary pull-right', 'title' => 'Save']) !!}
                </div>
            {!! Form::close() !!}
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->
@endsection
@section('scripts') 
    
    {!! Html::script('bower_components/AdminLTE/plugins/sparkline/jquery.sparkline.min.js') !!}
    {!! Html::script('bower_components/AdminLTE/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') !!}
    {!! Html::script('bower_components/AdminLTE/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') !!}
    {!! Html::script('bower_components/AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js') !!}
    
    <script>
        
    </script>
@endsection