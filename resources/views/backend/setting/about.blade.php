@extends('backend.template.master') 
@section('title','About')
@section('style')
    {!! Html::style('bower_components/AdminLTE/plugins/jvectormap/jquery-jvectormap-1.2.2.css') !!}
    <style type="text/css">
  .thumb
  {
  height: 300px;
  }
  .remove_img_preview
  {
  position:absolute;
  top:13px;
  right:3px;
  background-image:url(close2.png);
  background-repeat: no-repeat;
  background-size: 100%;
  margin-bottom: -15px;
  color:white;
  border-radius:50px;
  font-size:0.9em;
  padding: 0 0.3em 0;
  width:30px;
  height:30px;
  text-align:center;
  cursor:pointer;
  z-index: 9999;
  }
  .remove_img_preview:before
  {
  content: "";
  }
</style>
@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li></a href="#"><i class="fa fa-gears"></i> Settings</a></li>
        <li class="active"> About</li>
    </ol>
@endsection
@section('page-header', 'About Management')
@section('content') 


<!-- /.row -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Edit About Us</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <div class="btn-group">
                        <button class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown"><i class="fa fa-wrench"></i></button>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="#">Action</a>
                            </li>
                            <li>
                                <a href="#">Another action</a>
                            </li>
                            <li>
                                <a href="#">Something else here</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">Separated link</a>
                            </li>
                        </ul>
                    </div>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            {!! Form::model($about,$form) !!}
                <div class="box-body">
                    <div class="row">
                      <div class="col-md-8">
                        <div class="form-group">
                          <label class="control-label" for="inputError">Short Description</label>
                          {!! Form::textarea('short_description',null,['class'=>'form-control','rows'=>3]) !!}
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="inputError">Description</label>
                            {!! Form::textarea('description',null,['class'=>'form-control','id'=>'editor1','rows'=>10]) !!}
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                            {!! Form::label(null, 'Image', ['class' => 'control-label']) !!}
                            <div id="hide">
                              @if($about->image)
                                <img src="{{ link_to_images($about->image)}}" style="width: 100%">
                              @else
                                <img src="{{ link_to_images('noimage.gif')}}" style="width: 100%">
                              @endif
                            </div>
                            <div id="image_preview"></div>
                            <div>
                                <input type="file" name="image" id="image_upload" class="form-control">
                                <span id="helpBlock" class="help-block">
                                    <i>Format [JPG/PNG] | Max [2Mb]</i>
                                </span>
                            </div>
                        </div>
                      </div>
                    </div>
                </div>
                <div class="box-footer">
                    {!! link_to_action('Backend\DashboardController@index', 'Back', [], ['class' => 'btn btn-default']).' '.Form::submit('Save', ['class' => 'btn btn-primary pull-right', 'title' => 'Save']) !!}
                </div>
            {!! Form::close() !!}
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->
@endsection
@section('scripts') 
    
    {!! Html::script('bower_components/AdminLTE/plugins/sparkline/jquery.sparkline.min.js') !!}
    {!! Html::script('bower_components/AdminLTE/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') !!}
    {!! Html::script('bower_components/AdminLTE/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') !!}
    {!! Html::script('bower_components/AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js') !!}
    
    <script>
        function handleFileSelect(event) {
            var input = this;
            if (input.files && input.files.length) {
                var reader = new FileReader();
                this.enabled = false
                reader.onload = (function (e) {
                    $("#hide").hide();
                    $("#image_preview").html(['<div class="remove_img_preview"></div><img style="width:100%" class="thumb" src="', e.target.result, '" title="', escape(e.name), '"/>'].join(''))
                });
            reader.readAsDataURL(input.files[0]);
            }
        }
        $('#image_upload').change(handleFileSelect);
        $('#image_preview').on('click', '.remove_img_preview', function () {
            $("#image_preview").empty();
            $("#image_dashboard").val("");
            $("#hide").show();
        });
    </script>
    {!! Html::script('plugins/ckeditor/ckeditor.js') !!}
    <script type="text/javascript">
        var route_prefix = "{{ url(config('lfm.prefix')) }}";
        {!! \File::get(base_path('vendor/unisharp/laravel-filemanager/public/js/lfm.js')) !!}
        $('#lfm').filemanager('image', {prefix: route_prefix});
        $(function () {
            CKEDITOR.replace( 'editor1');
        }); 
    </script>
@endsection