@extends('backend.template.master') 
@section('title','About')
@section('style')
    {!! Html::style('bower_components/AdminLTE/plugins/jvectormap/jquery-jvectormap-1.2.2.css') !!}
@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li></a href="{{ route('admin.service') }}"><i class="fa fa-gears"></i> Project</a></li>
        <li class="active"> {{ $title }}</li>
    </ol>
@endsection
@section('page-header','Project') 
@section('content') 


<!-- /.row -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">{{ $title }}</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            {!! Form::model($data, $form) !!}
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                {!! Form::label('ltitle', 'Title', ['class' => 'control-label']) !!}
                                {!! Form::text('title', null, ['class' => 'form-control']) !!}
                                {!! Form::errorMsg('title') !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('lcategory', 'Category', ['class' => 'control-label']) !!}
                                {!! Form::text('category', null, ['class' => 'form-control']) !!}
                                {!! Form::errorMsg('category') !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('lSdescription', 'Caption', ['class' => 'control-label']) !!}
                                {!! Form::textarea('short_description', null, ['class' => 'form-control' ,'rows'=>5]) !!}
                                {!! Form::errorMsg('short_description') !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('ldescription', 'Description', ['class' => 'control-label']) !!}
                                {!! Form::textarea('description', null, ['class' => 'form-control','id'=>'editor1' ,'rows'=>10]) !!}
                                {!! Form::errorMsg('description') !!}
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                              <label class="control-label" for="inputError">Image</label>
                              <input type="file" name="image" class="form-control" id="imageUpload" onchange="makeFileList()">
                            </div>
                            <div class="form-group has-error">
                              <ul id="fileList"><li>No Files Selected</li></ul>
                              <div id="image_preview" class='row'></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    {!! link_to_action('Backend\ServiceController@index', 'Back', [], ['class' => 'btn btn-default']).' '.Form::submit('Save', ['class' => 'btn btn-primary pull-right', 'title' => 'Save']) !!}
                </div>
            {!! Form::close() !!}
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->
@endsection
@section('scripts') 
    
    {!! Html::script('bower_components/AdminLTE/plugins/sparkline/jquery.sparkline.min.js') !!}
    {!! Html::script('bower_components/AdminLTE/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') !!}
    {!! Html::script('bower_components/AdminLTE/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') !!}
    {!! Html::script('bower_components/AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js') !!}
    {!! Html::script('plugins/ckeditor/ckeditor.js') !!}
    <script type="text/javascript">
        var route_prefix = "{{ url(config('lfm.prefix')) }}";
        {!! \File::get(base_path('vendor/unisharp/laravel-filemanager/public/js/lfm.js')) !!}
        $('#lfm').filemanager('image', {prefix: route_prefix});
        $(function () {
            CKEDITOR.replace( 'editor1');
        }); 
    </script>
    <script type="text/javascript">
        
        function makeFileList(event){
            var input = document.getElementById('imageUpload');
            var imagePreview = document.getElementById('image_preview');
            if (input.files && input.files.length) {
                for (var x = 0; x < input.files.length; x++) {
                    var reader = new FileReader();
                    reader.onload = function(event) {
                        $(imagePreview).append('<div class="col-md-4 col-xs-6 col-preview"><div class="remove-img-preview" onclick="removePreview()"></div><div class="img-preview"><img class="img-responsive img-thumbnail img-rounded" src="'+event.target.result+'"></div></div>');
                    }
                    reader.readAsDataURL(input.files[x]);
                }
            }
        }
    </script>
@endsection