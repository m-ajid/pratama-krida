@extends('backend.template.master') 
@section('title','About')
@section('style')
    {!! Html::style('bower_components/AdminLTE/plugins/jvectormap/jquery-jvectormap-1.2.2.css') !!}
@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li></a href="#"><i class="fa fa-gears"></i> Settings</a></li>
        <li class="active"> About</li>
    </ol>
@endsection
@section('content') 


<!-- /.row -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Monthly Recap Report</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <div class="btn-group">
                        <button class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown"><i class="fa fa-wrench"></i></button>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="#">Action</a>
                            </li>
                            <li>
                                <a href="#">Another action</a>
                            </li>
                            <li>
                                <a href="#">Something else here</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">Separated link</a>
                            </li>
                        </ul>
                    </div>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            {!! Form::model($data, $form) !!}
                <div class="box-body">
                    <div class="form-group has-error">
                      <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> Input with
                        error</label>
                      <input type="text" class="form-control" id="inputError" placeholder="Enter ...">
                      <span class="help-block">Help block with error</span>
                    </div>
                    <div class="form-group has-error">
                      <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> Input with
                        error</label>
                      <textarea id="editor1" class="form-control" name="editor1" rows="10" cols="80">
                                            This is my textarea to be replaced with CKEditor.
                    </textarea>
                      <span class="help-block">Help block with error</span>
                    </div>
                   
                </div>
                <div class="box-footer">
                    {!! link_to_action('Backend\MessageController@index', 'Back', [], ['class' => 'btn btn-default']).' '.Form::submit('Save', ['class' => 'btn btn-primary pull-right', 'title' => 'Save']) !!}
                </div>
            {!! Form::close() !!}
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->
@endsection
@section('scripts') 
    
    {!! Html::script('bower_components/AdminLTE/plugins/sparkline/jquery.sparkline.min.js') !!}
    {!! Html::script('bower_components/AdminLTE/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') !!}
    {!! Html::script('bower_components/AdminLTE/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') !!}
    {!! Html::script('bower_components/AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js') !!}
    <script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
    <script>
        $(function () {
        CKEDITOR.replace('editor1');
      });
    </script>
@endsection