@extends('backend.template.master') 
@section('title','About')
@section('style')
    {!! Html::style('bower_components/AdminLTE/plugins/jvectormap/jquery-jvectormap-1.2.2.css') !!}
@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li></a href="#"><i class="fa fa-gears"></i> Settings</a></li>
        <li class="active"> About</li>
    </ol>
@endsection
@section('content') 

<!-- /.row -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Monthly Recap Report</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
              {!! $dataTable->table(['class'=>'table table-hover table-bordered table-condensed table-responsive table-striped data-table','cellspacing'=>'0'],true) !!}
            </div>
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->
@endsection
@section('scripts') 
    
    {!! Html::script('bower_components/AdminLTE/plugins/sparkline/jquery.sparkline.min.js') !!}
    {!! Html::script('bower_components/AdminLTE/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') !!}
    {!! Html::script('bower_components/AdminLTE/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') !!}
    {!! Html::script('bower_components/AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js') !!}
    {!! $dataTable->scripts() !!}
    <script type="text/javascript">
        $(function () {
            $('#example1').DataTable();
        }); 
    </script>
  
@endsection