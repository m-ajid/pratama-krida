@extends('frontend.template.master') 
@section('title','Home')
@section('style')
  <style type="text/css">
    .portfolio-modal .close-modal .lr {
        height: 35px;
    }
    .portfolio-modal .close-modal .lr .rl {
        height: 35px;
    }
    #services-section img {
        display: inline-block;
        font-size: 40px;
        border: 0;
        width: 100px;
        height: 80px;
        padding: 12px 15px;
        background: #032934;
        margin-bottom: 6px;
        border-radius: 10%;
        color: #f3ca27;
        transition: all 0.5s;
    }
    .portfolio-item .hover-bg {
      height: auto !important;
    }
    .img-carousel{
      width: 100%;
      margin-left: 0px;
      margin-right: 0px;
    }
    .carousel-description{
      z-index: 99999;
      position: absolute !important;
      margin-bottom: auto;
      bottom: 50px;
      left: 0;
      right: 0;
    }
    .item{
      width: 100% !important;
      margin-right: -10px;
    }
    .header-carousel{
      margin-left: 0px !important;
      margin-right: 0px !important;
    }
    .owl-pagination{
      position: absolute;
      margin:auto;
      left: 0;
      right: 0;
    }
    .owl-theme .owl-controls{
      margin-top: -35px !important;
    }
  </style>
@endsection
@section('content') 
  @if($hasCarousel)
  <div id="slider-section">
    <header class="text-center" name="home">
      <!-- <div class="intro-text"> -->
        <div class="header-carousel row">
          @foreach($hasCarousel as $carousel)
            <div class="item">
              <img src="{{ link_to_carousels($carousel->image) }}" class="img-carousel">
              <div class="carousel-description">  
              <h1 class="wow fadeInDown"><strong><span class="color">{{ $carousel->title }}</span></strong></h1>
              <p class="wow fadeInDown"><?php echo html_entity_decode($carousel->description, ENT_QUOTES, "utf-8"); ?></p>
              <a href="#works-section" class="btn btn-default btn-lg page-scroll wow fadeInUp" data-wow-delay="200ms">Our Services</a> 
              </div>
            </div>
          @endforeach
        </div>
      <!-- </div> -->
    </header>
  </div>
  @endif
  <div id="about-section">
    <div class="container">
      <div class="section-title text-center wow fadeInDown">
        <h2><strong>Tentang Kami</strong></h2>
        <hr>
        <div class="clearfix"></div>
        <p class="text-justify">{{ $hasAbout['short_description']}}</p>
      </div>
      <div class="row">
        <div class="col-md-5 wow fadeInLeft"> <img src="{{ link_to_images($hasAbout['image'])}}" class="img-responsive"> </div>
        <div class="col-md-7 text-justify wow fadeInRight">
          <?php echo html_entity_decode($hasAbout['description'], ENT_QUOTES, "utf-8"); ?>
        </div>
      </div>
    </div>
  </div>
  @if(count($hasService)>=1)
    <div id="services-section" class="text-center">
      <div class="container">
        <div class="section-title wow fadeInDown">
          <a href="{{ route('service') }}"><h2><strong>Layanan Kami</strong></h2></a>
          <hr>
          <div class="clearfix"></div>
          <p class="text-justify">Lingkup pelayanan PT. PRATAMA KRIDA secara umum dapat dibagi dua yaitu lingkup layanan konsultansi konstruksi dan konsultansi non konstruksi. Masing-masing lingkup layanan mulai dari jasa survey, perencanaan, perancangan, desain, studi, penelitian dan terapan, bantuan teknik, konsultansi manajemen, sistem informasi dan jasa khusus lainnya. Berdasarkan kelompok bidang yang dapat ditangani, maka cakupan pelayanan jasa konsultansi yang disediakan oleh PT. PRATAMA KRIDA meliputi pekerjaan :</p>
        </div>
        <div class="space"></div>
        <div class="row">
            @foreach($hasService as $service)
              <div class="col-md-3 col-sm-6 service wow fadeInUp" data-wow-delay="200ms" style="height:300px">
                @if($service['image']='')
                  <img class="img-responsive img-service" src="{{ asset('storage/images/service').'/'.$service['image'] }}">
                @else
                  <img class="img-responsive img-service" src="{{ asset('storage/images').'/default.png' }}">
                @endif

                <h4><strong>{{ $service['title'] }}</strong></h4>
                <p class="text-justify"><?php echo html_entity_decode($service['short_description'], ENT_QUOTES, "utf-8"); ?></p>
            </div>
            @endforeach
        </div>
      </div>
    </div>
  @endif
  @if(count($hasProject)>=1)
  <div id="project-section" class="text-center">
    <div class="container"> <!-- Container -->
      <div class="section-title wow fadeInDown">
        <h2><strong>Projek Kami</strong></h2>
        <hr>
        <!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed dapibus leo nec ornare diamcommodo nibh ante facilisis.</p> -->
      </div>
      <div class="row">
        <div class="portfolio-items">
          @foreach($hasProject as $project)
          <div class="col-sm-6 col-md-3 col-lg-3 project2">
            <div class="portfolio-item wow fadeInUp" data-wow-delay="200ms">
              <div class="hover-bg"> <a href="#portfolioModal1" class="portfolio-link" data-toggle="modal"></a>
                <div class="hover-text">
                  <h4>{{ $project['title'] }}</h4>
                  {{ $project['short_description'] }}
                  <div class="clearfix"></div>
                  <i class="fa fa-plus"></i> 
                </div>
                  @if($project['image']='')
                  <img class="img-responsive img-service" src="{{ asset('storage/images/project').'/'.$project['image'] }}">
                @else
                  <img class="img-responsive img-service" src="{{ asset('storage/images').'/default.png' }}">
                @endif 
              </div>
            </div>
        </div>
        @endforeach
      </div>
    </div>
  </div>
  </div>
  @endif
  @if($hasGallery)
    <div id="gallery-section" class="text-center">
      <div class="container"> <!-- Container -->
        <div class="section-title wow fadeInDown">
          <a href="{{ route('gallery') }}"><h2><strong>Galleri</strong></h2></a>
          <hr>
          <div class="clearfix"></div>
          <!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed dapibus leo nec ornare diamcommodo nibh ante facilisis.</p> -->
        </div>
        <div class="categories">
          <ul class="cat">
            <li>
              <ol class="type">
                <li><a href="#" data-filter="*" class="active no-redirect">Foto</a></li>
                @foreach($hasGallery as $key => $gallery)
                  <li><a href="#" class="no-redirect" data-filter=".{{ str_replace(' ', '-', $gallery['label'])}}">{{ $gallery['label']}}</a></li>
                @endforeach
                <li><a href="{{ route('gallery') }}" data-filter="" class="redirect">Tampilkan Semua</a></li>
              </ol>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="row">
          <div class="portfolio-items">
            @foreach($hasGallery as $key => $gallery)
                <?php $images=explode(',',$gallery['image']);?>
                @foreach($images as $key => $image)
                  <div class="col-sm-6 col-md-3 col-lg-3 {{ str_replace(' ', '-', $gallery['label'])}}">
                    <div class="portfolio-item wow fadeInUp" data-wow-delay="400ms">
                      <div class="hover-bg"> <a href="#{{ str_replace(' ', '-', $gallery['title'])}}{{ $key+1}}" class="portfolio-link" data-toggle="modal">
                        <div class="hover-text">
                          <h4>{{ $gallery['title']}}</h4>
                          {{ $gallery['description']}}
                          <div class="clearfix"></div>
                          <i class="fa fa-plus"></i> 
                        </div>
                        @if (substr($image,-4)=='.mp4')
                          <video width="100%" controls>
                            <source src="{{ link_to_gallery($gallery['label'],$image)}}" type="video/mp4">
                            <source src="{{ link_to_gallery($gallery['label'],str_replace('.mp4','.ogg',$image))}}" type="video/ogg">
                              Your browser does not support HTML5 video.
                          </video>
                        @else
                          <img src="{{ link_to_gallery($gallery['label'],$image)}}" class="img-responsive img-thumbnail img-rounded" alt="Project Title">  
                        @endif
                        </a>
                      </div>
                    </div>
                  </div>
                @endforeach
            @endforeach
            </div>
        </div>
      </div>
    </div>
  @endif
 @include('frontend.template.partials.modal-gallery')
  @if($hasTestimony)
    <div id="testimonials-section" class="text-center">
      <div class="container">
        <div class="section-title wow fadeInDown">
          <h2><strong>Testimoni</strong></h2>
          <hr>
        </div>
        <div class="row">
          <div class="col-md-8 col-md-offset-2">
            <div id="testimonial" class="owl-carousel owl-theme wow fadeInUp" data-wow-delay="200ms">
              @foreach($hasTestimony as $key => $testimony)
                  <div class="item">
                    <?php echo html_entity_decode($testimony['description'], ENT_QUOTES, "utf-8"); ?>
                    <p><strong>{{ $testimony['name']}}</strong>{{ $testimony['company']}}</p>
                  </div>
              @endforeach
            </div>
          </div>
        </div>
      </div>
    </div>
  @endif

  <div id="contact-section" class="text-center">
    <div class="container">
      <div class="section-title wow fadeInDown">
        <h2><strong>Hubungi Kami</strong></h2>
        <hr>
        <!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed dapibus leo nec ornare diamcommodo nibh ante facilisis.</p> -->
      </div>
      <div class="col-md-6 wow fadeInLeft" data-wow-delay="400ms">
        <h3>Head Office</h3>
        @if($hasContact)
        <div class="contact-detail">
          <ul>
            <li><i class="fa fa-building-o fa-lg"></i> {{$hasContact['address'] }}</li>
            <li><i class="fa fa-envelope-o fa-lg"></i>{{$hasContact['email'] }}</li>
            <li><i class="fa fa-phone fa-lg"></i> {{$hasContact['phone'] }}</li>
            <li><i class="fa fa-comments-o fa-lg"></i>{{$hasContact['mobile'] }}</li>
            <li><i class="fa fa-map-marker fa-lg"></i> <div class="contact-map"><iframe src="{{$hasContact['map'] }}" width="400" height="200" frameborder="0" style="border:0" allowfullscreen></iframe></div></li>
          </ul>
        </div>
        @endif
        <div class="clearfix"></div>
      </div>
      <div class="col-md-6 wow fadeInRight" data-wow-delay="400ms">
        <h3>Tinggalkan Pesan</h3>
            @if( Session::has( 'success' ))
                <div class="alert alert-info alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Sukses!</strong>Pesan Sudah terkirim
                </div>
            @elseif( Session::has( 'error' ))
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Gagal!</strong>Pesan tidak terkirim
                </div>
            @endif
        {!! Form::open(['id'=>'messageForm','route'=>'message.store']) !!}
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <input type="text" name="name" id="name" class="form-control" placeholder="Nama" required="required">
                <p class="help-block text-danger"></p>
              </div>
            </div>
            <div class="col-md-8">
              <div class="form-group">
                <input type="email" name="email" id="email" class="form-control" placeholder="Email" required="required">
                <p class="help-block text-danger"></p>
              </div>
            </div>
          </div>
          <div class="form-group">
            <textarea name="message" id="message" class="form-control" rows="4" placeholder="Pesan" required></textarea>
            <p class="help-block text-danger"></p>
          </div>
          <div id="success"></div>
          <button type="submit" class="btn btn-default ">Kirim</button>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
@endsection
@section('script')
  <script type="text/javascript">
    $('.redirect').on('click',function(){
        console.log($(this).attr('href'));
        window.location=$(this).attr('href');
    })
  </script>
@endsection