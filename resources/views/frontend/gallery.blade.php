@extends('frontend.template.master') 
@section('title','Gallery')
@section('style')
  <style type="text/css">
    .portfolio-modal .close-modal .lr {
        height: 35px;
    }
    .portfolio-modal .close-modal .lr .rl {
        height: 35px;
    }
    .bg-hover{
      position: absolute;
      z-index: -1;
    }
    .portfolio-item .hover-bg {
      height: auto !important;
    }
  </style>
@endsection
@section('content') 
  @if($hasGallery)
    <div id="slider-section">
    <header class="text-center" name="home">
      <div class="intro-text">
        <div class="header-carousel row">
          @foreach($hasGallery as $gallery)
            <?php $images=explode(',',$gallery['image']);?>
                @foreach($images as $key => $image)
            <div class="item col-sm-8 col-md-offset-2">
              <h1 class="wow fadeInDown"><strong><span class="color">{{ $gallery->title }}</span></strong></h1>
              <p class="wow fadeInDown"><?php echo html_entity_decode($gallery->description, ENT_QUOTES, "utf-8"); ?></p>
              <a href="#works-section" class="btn btn-default btn-lg page-scroll wow fadeInUp" data-wow-delay="200ms">Our Services</a> 
            </div>
          @endforeach
          @endforeach
        </div>
      </div>
    </header>
  </div>
    <div id="gallery-section" class="text-center">
      <div class="container"> <!-- Container -->
        <div class="section-title wow fadeInDown">
          <h2><strong>Gallery</strong></h2>
          <hr>
          <div class="clearfix"></div>
          <!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed dapibus leo nec ornare diamcommodo nibh ante facilisis.</p> -->
        </div>
        <div class="categories">
          <ul class="cat">
            <li>
              <ol class="type">
                <li><a href="#" data-filter="*" class="all active">Foto</a></li>
                <li><a href="#" class="album-select">Album</a></li>
              </ol>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="row">
          <div id="album-group" class="hide">
            @foreach($hasGallery as $key => $gallery)
                <?php $images=explode(',',$gallery['image']);
                  $imagePrev=array_rand($images,1);
                  $imageHover=array_rand($images,1);
                ?>
                  <div class="col-sm-6 col-md-3 col-lg-3 album">
                    <div class="portfolio-item wow fadeInUp" data-wow-delay="10ms">
                      <div class="hover-bg"> 
                      <a class="album-filter" href="#album-group" data-filter="{{ str_replace(' ', '-', $gallery['label'])}}">
                        <div class="hover-text">
                          <h4>{{ $gallery['title']}}</h4>
                          {{ $gallery['description']}}
                          <div class="clearfix"></div>
                          <i class="fa fa-plus"></i> 
                        </div>
                        @if (substr($images[$imagePrev],-4)=='.mp4')
                          <video width="100%" controls>
                            <source src="{{ link_to_gallery($gallery['label'],$images[$imagePrev])}}" type="video/mp4">
                            <source src="{{ link_to_gallery($gallery['label'],str_replace('.mp4','.ogg',$images[$imagePrev]))}}" type="video/ogg">
                              Your browser does not support HTML5 video.
                          </video>
                        @else
                          <img src="{{ link_to_gallery($gallery['label'],$images[$imagePrev])}}" class="img-responsive img-thumbnail img-rounded" alt="Project Title">
                        @endif
                        <h4>{{ $gallery['label'] }}</h4> </a> 
                      </div>
                    </div>
                  </div>
            @endforeach
          </div>
          <div class="portfolio-items">
            @foreach($hasGallery as $key => $gallery)
                <?php $images=explode(',',$gallery['image']);?>
                @foreach($images as $key => $image)
                  <div class="col-sm-6 col-md-3 col-lg-3 {{ str_replace(' ', '-', $gallery['label'])}}">
                    <div class="portfolio-item wow fadeInUp" data-wow-delay="{{ $key.'0'}}">
                      <div class="hover-bg"> <a href="#{{ str_replace(' ', '-', $gallery['title'])}}{{ $key+1}}" class="portfolio-link" data-toggle="modal">
                        <div class="hover-text">
                          <h4>{{ $gallery['title']}}</h4>
                          {{ $gallery['description']}}
                          <div class="clearfix"></div>
                          <i class="fa fa-plus"></i> 
                        </div>
                        @if (substr($image,-4)=='.mp4')
                          <video width="100%" controls>
                            <source src="{{ link_to_gallery($gallery['label'],$image)}}" type="video/mp4">
                            <source src="{{ link_to_gallery($gallery['label'],str_replace('.mp4','.ogg',$image))}}" type="video/ogg">
                              Your browser does not support HTML5 video.
                          </video>
                        @else
                          <img src="{{ link_to_gallery($gallery['label'],$image)}}" class="img-responsive img-thumbnail img-rounded" alt="Project Title"> 
                        @endif
                        </a> 
                      </div>
                    </div>
                  </div>
                @endforeach
            @endforeach
            </div>
            <div id="album-group-other" class="hide">
            <h4>Album Lainnya</h4>
            <hr>
            @foreach($hasGallery as $key => $gallery)
                <?php $images=explode(',',$gallery['image']);
                  $imagePrev=array_rand($images,1);
                  $imageHover=array_rand($images,1);
                ?>
                  <div class="col-sm-6 col-md-3 col-lg-3" id="{{ str_replace(' ', '-', $gallery['label'])}}">
                    <div class="portfolio-item wow fadeInUp" data-wow-delay="10ms">
                      <div class="hover-bg"> 
                      <a class="album-filter" href="#album-group" data-filter="{{ str_replace(' ', '-', $gallery['label'])}}">
                        <div class="hover-text">
                          <h4>{{ $gallery['title']}}</h4>
                          {{ $gallery['description']}}
                          <div class="clearfix"></div>
                          <i class="fa fa-plus"></i> 
                        </div>
                        @if (substr($images[$imagePrev],-4)=='.mp4')
                          <video width="100%" controls>
                            <source src="{{ link_to_gallery($gallery['label'],$images[$imagePrev])}}" type="video/mp4">
                            <source src="{{ link_to_gallery($gallery['label'],str_replace('.mp4','.ogg',$images[$imagePrev]))}}" type="video/ogg">
                              Your browser does not support HTML5 video.
                          </video>
                        @else
                          <img src="{{ link_to_gallery($gallery['label'],$images[$imagePrev])}}" class="img-responsive img-thumbnail img-rounded" alt="Project Title">
                        @endif
                        <h4>{{ $gallery['label'] }}</h4> </a> 
                      </div>
                    </div>
                  </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
  @endif
 @include('frontend.template.partials.modal-gallery')
@endsection
@section('script')
<script type="text/javascript">
  $(window).bind('scroll', function() {
        var navHeight = 100;
        if ($(window).scrollTop() > navHeight) {
            $('.navbar-default').addClass('on');
        } else {
            $('.navbar-default').removeClass('on');
        }
    });
  $('.album-select').click(function(){
      $('#album-group').removeClass('hide');
      $('.portfolio-items').addClass('hide');
      $(this).addClass('active');
  });
  $('.all').click(function(){
      $('#album-group').addClass('hide');
      $('.portfolio-items').removeClass('hide');
  });
  var $container = $('.portfolio-items');
  $('.album-filter').click(function() {
      $('#album-group').addClass('hide');
      $('#album-group-other').removeClass('hide');
      $('.portfolio-items').removeClass('hide');
      var selector = '.'+$(this).attr('data-filter');
      var unselect = '#'+$(this).attr('data-filter');
      $('#album-group-other .hide').removeClass('hide');
      $(unselect).addClass('hide');
      $container.isotope({
          filter: selector,
          animationOptions: {
              duration: 750,
              easing: 'linear',
              queue: false
          }
      });
      return false;
  });

</script>
@endsection