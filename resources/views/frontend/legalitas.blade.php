@extends('frontend.template.master') 
@section('title','Legalitas')
@section('style')
  <style type="text/css">
    .portfolio-modal .close-modal .lr {
        height: 35px;
    }
    .portfolio-modal .close-modal .lr .rl {
        height: 35px;
    }
    #legalitas-section {
        padding: 80px 0 100px 0;
        /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#032731+0,032e3a+20,032e3a+100,032731+100 */
        background: rgb(3,39,49); /* Old browsers */
        background: -moz-linear-gradient(top, rgba(3,39,49,1) 0%, rgba(3,46,58,1) 20%, rgba(3,46,58,1) 100%, rgba(3,39,49,1) 100%); /* FF3.6-15 */
        background: -webkit-linear-gradient(top, rgba(3,39,49,1) 0%, rgba(3,46,58,1) 20%, rgba(3,46,58,1) 100%, rgba(3,39,49,1) 100%); /* Chrome10-25,Safari5.1-6 */
        background: linear-gradient(to bottom, rgba(3,39,49,1) 0%, rgba(3,46,58,1) 20%, rgba(3,46,58,1) 100%, rgba(3,39,49,1) 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#032731', endColorstr='#032731', GradientType=0 ); /* IE6-9 */
        color: #5b777f;
    }
    #legalitas-section img {
        display: inline-block;
        font-size: 40px;
        border: 0;
        width: 100px;
        height: 80px;
        padding: 12px 15px;
        background: #032934;
        margin-bottom: 6px;
        border-radius: 10%;
        color: #f3ca27;
        transition: all 0.5s;
    }
    embed{
      width:100%;
      height: 700px;
    }
    .nav-tabs{
      border: none;
    }
    /*.nav-tabs.nav-justified>li>a{
      border: none;
    }*/
    ul.type li a:hover {
        background: #f7dc6f;
    }
    ul.type  li a {
        color: #51633d;
        border: 1px solid #f7dc6f;
        padding: 8px 16px;
        border-radius: 3px !important;
    }
    ul.type li {
    display: inline-block;
    margin-left: 20px;
    padding: 10px;
    }
  </style>
@endsection
@section('content') 
  <div id="legalitas-section" class="text-center">
      <div class="container">
        <div class="section-title wow fadeInDown">
          <h2>Our <strong>Legalitas</strong></h2>
          <hr>
          <div class="clearfix"></div>
          <!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed dapibus leo nec ornare diamcommodo nibh ante facilisis.</p> -->
        </div>
        <div class="categories">
            <ul class="type nav nav-tabs nav-justified">
              <li><a href="#legalitas" data-toggle="tab" class="active">Legalitas</a></li>
                <li><a href="#Akte-No-1" data-toggle="tab">Akte-No.1</a></li>
                <li><a href="#Akte-No-2" data-toggle="tab" class="">Akte-No.2</a>
                <li><a href="#Akte-No-24" data-toggle="tab" class="">Akte-No.24</a>
              </li>
            </ul>
          <div class="clearfix"></div>
        </div>
        <div class="tab-content">
          <div class="col-sm-12 col-md-12 col-lg-12 tab-pane fade in active" id="legalitas">
                <embed src="{{asset('storage/files/Legalitas.pdf')}}" width="500" height="375" type='application/pdf'>
          </div>
          <div class="col-sm-12 col-md-12 col-lg-12 tab-pane fade" id="Akte-No-1">
                <embed src="{{asset('storage/files/Akte-No.1-Tgl-01-07-2009.pdf')}}" width="500" height="375" type='application/pdf'>
          </div>
          <div class="col-sm-12 col-md-12 col-lg-12 tab-pane fade" id="Akte-No-2">
                <embed src="{{asset('storage/files/Akte-No.2-Tgl-04-02-1980.pdf')}}" width="500" height="375" type='application/pdf'>
          </div>
          <div class="col-sm-12 col-md-12 col-lg-12 tab-pane fade" id="Akte-No-24">
                <embed src="{{asset('storage/files/Akte-No.24-Tgl-14-07-2014.pdf')}}" width="500" height="375" type='application/pdf'>
          </div>
        </div>
      </div>
  </div>
@endsection
@section('script')
<script type="text/javascript">
  $(window).bind('scroll', function() {
        var navHeight = 100;
        if ($(window).scrollTop() > navHeight) {
            $('.navbar-default').addClass('on');
        } else {
            $('.navbar-default').removeClass('on');
        }
    });
</script>
@endsection