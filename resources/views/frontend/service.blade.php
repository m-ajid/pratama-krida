@extends('frontend.template.master') 
@section('title','Services')
@section('style')
  <style type="text/css">
    .portfolio-modal .close-modal .lr {
        height: 35px;
    }
    .portfolio-modal .close-modal .lr .rl {
        height: 35px;
    }
    #legalitas-section {
        padding: 80px 0 100px 0;
        /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#032731+0,032e3a+20,032e3a+100,032731+100 */
        background: rgb(3,39,49); /* Old browsers */
        background: -moz-linear-gradient(top, rgba(3,39,49,1) 0%, rgba(3,46,58,1) 20%, rgba(3,46,58,1) 100%, rgba(3,39,49,1) 100%); /* FF3.6-15 */
        background: -webkit-linear-gradient(top, rgba(3,39,49,1) 0%, rgba(3,46,58,1) 20%, rgba(3,46,58,1) 100%, rgba(3,39,49,1) 100%); /* Chrome10-25,Safari5.1-6 */
        background: linear-gradient(to bottom, rgba(3,39,49,1) 0%, rgba(3,46,58,1) 20%, rgba(3,46,58,1) 100%, rgba(3,39,49,1) 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#032731', endColorstr='#032731', GradientType=0 ); /* IE6-9 */
        color: #5b777f;
    }
    #legalitas-section img {
        display: inline-block;
        font-size: 40px;
        border: 0;
        width: 100px;
        height: 80px;
        padding: 12px 15px;
        background: #032934;
        margin-bottom: 6px;
        border-radius: 10%;
        color: #f3ca27;
        transition: all 0.5s;
    }
    embed{
      width:100%;
      height: 700px;
    }
    .nav-tabs{
      border: none;
    }
    /*.nav-tabs.nav-justified>li>a{
      border: none;
    }*/
    ul.type li a:hover {
        background: #f7dc6f;
    }
    ul.type  li a {
        color: #51633d;
        border: 1px solid #f7dc6f;
        padding: 8px 16px;
        border-radius: 3px !important;
    }
    ul.type li {
    display: inline-block;
    margin-left: 20px;
    padding: 10px;
    }
    .service{
      margin-bottom: 25px;
    }
    #services-section i.fa {
         font-size: 25px; 
         border: 0; 
         width: inherit; 
         height: inherit; 
         padding: none; 
         background: none; 
         margin-bottom: none; 
         border-radius: none; 
         color: #f3ca27; 
         transition: none; 
    }
  </style>
@endsection
@section('content') 
  @if(count($hasService)>=1)
    <div id="services-section" class="text-center">
      <div class="container">
        <div class="section-title wow fadeInDown">
          <h2><strong>Layanan Kami</strong></h2>
          <hr>
          <div class="clearfix"></div>
          <p class="text-justify">Lingkup pelayanan PT. PRATAMA KRIDA secara umum dapat dibagi dua yaitu lingkup layanan konsultansi konstruksi dan konsultansi non konstruksi. Masing-masing lingkup layanan mulai dari jasa survey, perencanaan, perancangan, desain, studi, penelitian dan terapan, bantuan teknik, konsultansi manajemen, sistem informasi dan jasa khusus lainnya. Berdasarkan kelompok bidang yang dapat ditangani, maka cakupan pelayanan jasa konsultansi yang disediakan oleh PT. PRATAMA KRIDA meliputi pekerjaan :</p>
        </div>
        <div class="space"></div>
        <div class="row">
            @foreach($hasService as $key => $service)
              <div class="col-md-12 col-sm-12 service wow fadeInUp" data-wow-delay="200ms">
                  <h4 class="text-left"><strong>{{ $service['category'] }}</strong></h4>
                  @foreach($service['data'] as $value)
                    <div class="col-md-12 col-sm-12 service wow fadeInUp" data-wow-delay="200ms">
                      <h4 class="text-left"><strong>{{ $value['title'] }}</strong></h4>
                      <p class="text-justify"><?php echo html_entity_decode($value['short_description'], ENT_QUOTES, "utf-8"); ?></p>
                      <div class="media">
                        <div class="media-left">
                          @if($value['image']='')
                            <img class="media-object" src="{{ asset('storage/images/service').'/'.$value['image'] }}" style="width:60px">
                          @else
                            <img class="media-object" src="{{ asset('storage/images').'/default.png' }}" style="width:60px">
                          @endif
                        </div>
                        <div class="media-body">
                          <p class="text-justify"><?php echo html_entity_decode($value['description'], ENT_QUOTES, "utf-8"); ?></p>
                        </div>
                      </div>
                    </div>
                  @endforeach
              </div>
            @endforeach
        </div>
        <div class="space"></div>
      </div>
    </div>
  @endif
@endsection
@section('script')
<script type="text/javascript">
  $(window).bind('scroll', function() {
        var navHeight = 100;
        if ($(window).scrollTop() > navHeight) {
            $('.navbar-default').addClass('on');
        } else {
            $('.navbar-default').removeClass('on');
        }
    });
</script>
@endsection