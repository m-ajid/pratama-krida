<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Pratama Krida | @yield('title')</title>
    <meta name="robots" content="noindex, follow">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="https://www.templategarden.com/preview/tempo/template/img/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="https://www.templategarden.com/preview/tempo/template/img/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="https://www.templategarden.com/preview/tempo/template/img/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="https://www.templategarden.com/preview/tempo/template/img/apple-touch-icon-114x114.png">
    {!! Html::style('bower_components/AdminLTE/bootstrap/css/bootstrap.min.css') !!}
    {!! Html::style('bower_components/font-awesome/css/font-awesome.css') !!}
    {!! Html::style('css/owl.carousel.css') !!}
    {!! Html::style('css/owl.theme.css') !!}
    {!! Html::style('css/style.css') !!}
    {!! Html::style('css/animate.min.css') !!}
    {!! Html::style('http://fonts.googleapis.com/css?family=Lato:400,700,900,300') !!}
    {!! Html::style('http://fonts.googleapis.com/css?family=Open+Sans:400,700,800,600,300') !!}
    @yield('style')
    <style type="text/css">
        .navbar-img {
            display: inline-block;
            font-size: inherit;
            text-rendering: auto;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
            height: 45px;
        }
        .logo{
            padding: 3px !important;
            background: #fff;
            border-radius: 3px;
            margin-right: 5px;
        }
    </style>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
  </head>
  <body>
    <div id="preloader">
      <div id="status"> <img src="{{ asset('storage/images/ajax-loader.gif') }}" height="64" width="64" alt=""> </div>
    </div>
    <section id="navbar-section">
        @if(Route::current()->getName() == 'home')
            @include('frontend.template.partials.navbar') 
        @else
            @include('frontend.template.partials.navbar2') 
        @endif
     
    </section id="main-section">
    <section id="footer-section">
      @yield('content')
    </section>
    <section>
     @include('frontend.template.partials.footer') 
    </section>

    <!-- Portfolio Modals --> 
    <!-- Portfolio Modal 1 -->
   
    
    <!-- Portfolio Modal 2 -->
    
    {!! Html::script('js/jquery-1.11.1.min.js') !!} 
    {!! Html::script('bower_components/AdminLTE/bootstrap/js/bootstrap.min.js') !!}
    <!-- <script type="text/javascript" src="{{ asset('bower_components/AdminLTE/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>  -->
    <script type="text/javascript" src="{{ asset('js/SmoothScroll.js') }}"></script> 
    {!! Html::script('js/wow.min.js') !!}
    {!! Html::script('js/jquery.isotope.js') !!} 
    {!! Html::script('js/owl.carousel.js') !!} 
    {!! Html::script('js/modernizr.custom.js') !!}
    {!! Html::script('js/main.js') !!}
    @yield('script')
    <script type="text/javascript">
      $(".header-carousel").owlCarousel({
        items:1,
        startPosition:0,
        nav:true,
        loop:true,
        margin:10,
        autoplay:true,
        autoplayTimeout:1000,
        autoplayHoverPause:false
      });
    </script>
  </body>
</html>