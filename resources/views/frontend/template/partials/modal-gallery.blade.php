@foreach($hasGallery as $key => $gallery)
      <?php $images=explode(',',$gallery['image']);?>
      @foreach($images as $key => $image)
        <div class="portfolio-modal modal fade" id="{{str_replace(' ', '-', $gallery['title'])}}{{ $key+1}}" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
              <div class="lr">
                <div class="rl"> </div>
              </div>
            </div>
            <div class="container">
              <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                  <div class="modal-body"> 
                    <!-- Project Details Go Here -->
                    <h2>{{ $gallery['title']}}</h2>
                    @if (substr($image,-4)=='.mp4')
                          <video width="100%" controls>
                            <source src="{{ link_to_gallery($gallery['label'],$image)}}" type="video/mp4">
                            <source src="{{ link_to_gallery($gallery['label'],str_replace('.mp4','.ogg',$image))}}" type="video/ogg">
                              Your browser does not support HTML5 video.
                          </video>
                    @else
                      <img src="{{ link_to_gallery($gallery['label'],$image)}}" class="img-responsive img-thumbnail img-rounded" alt="Project Title"> </a> 
                    @endif
                    <p>{{ $gallery['description']}}</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      @endforeach
    @endforeach