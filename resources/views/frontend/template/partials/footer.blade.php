<div id="footer">
  <div class="container">
      <div class="col-sm-7 wow fadeInUp" data-wow-delay="400ms">
        <div class="footer-menu">
          <ul>
            <li><a href="{{ route('home') }}#home" class="page-scroll">Beranda</a></li>
            <li><a href="{{ route('home') }}#about-section" class="page-scroll">Tentang Kami</a></li>
            <li><a href="{{ route('service') }}" class="page-scroll">Layanan</a></li>
            <!-- <li><a href="#project-section" class="page-scroll">Our Project</a></li> -->
            <li><a href="{{ route('gallery') }}" class="page-scroll">Galleri</a></li>
            <li><a href="{{ route('home') }}#testimonials-section" class="page-scroll">Testimoni</a></li>
            <li><a href="{{ route('home') }}#contact-section" class="page-scroll">Hubungi Kami</a></li>
          </ul>
        </div>
      </div>
      <div class="col-sm-5 wow fadeInUp" data-wow-delay="400ms">
        @if($hasFooter)
          <div class="footer-social">
            <ul>
              <li><a href="#">Connect with us on </a></li>
              <li><a href="{{ $hasFooter['facebook']}}"><i class="fa fa-facebook"></i></a></li>
              <li><a href="{{ $hasFooter['twitter']}}"><i class="fa fa-twitter"></i></a></li>
              <li><a href="{{ $hasFooter['instagram']}}"><i class="fa fa-instagram"></i></a></li>
              <li><a href="{{ $hasFooter['linked_in']}}"><i class="fa fa-linkedin"></i></a></li>
            </ul>
          </div>
        @endif
      </div>
      <p class="text-center wow fadeInUp" data-wow-delay="600ms">Copyright © 2017. <strong>Pratama Krida</strong></p>
  </div>
</div>