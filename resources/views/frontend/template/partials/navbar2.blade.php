
<!-- Navigation
    ==========================================-->
<nav id="menu" class="navbar navbar-default navbar-fixed-top">
  <div class="container"> 
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <a class="navbar-brand logo" href="/">
        <img class="img-responsive navbar-img" src="{{ asset('storage/images/logo-pk.jpg') }}">
      </a>
      <a class="navbar-brand" href="/"><strong>PRATAMA KRIDA</strong></a> </div>
    
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="{{ route('home') }}" class="page-scroll">Dashboard</a></li>
        <li><a href="{{ route('home') }}#about-section" class="page-scroll">Tentang Kami</a></li>
        <li><a href="{{ route('service') }}" class="page-scroll">Layanan</a></li>
        <!-- <li><a href="{{ route('home') }}" class="page-scroll">Our Project</a></li> -->
        <li><a href="{{ route('gallery') }}" class="page-scroll">Galleri</a></li>
        <li><a href="{{ route('home') }}#testimonials-section" class="page-scroll">Testimoni</a></li>
        <li><a href="{{ route('home') }}#contact-section" class="page-scroll">Hubungi Kami</a></li>
      </ul>
    </div>
    <!-- /.navbar-collapse --> 
  </div>
  <!-- /.container-fluid --> 
</nav>