<?php

use Illuminate\Database\Seeder;
use App\Models\User;
class UsersTableSeeder extends Seeder
{
    protected $roleSlugsArray = [
        'sales-admin', 'sales-representative', 'finance-admin', 'shop-admin', 'editor-admin',
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();
        // DB::statement('TRUNCATE users CASCADE');
        DB::table('activations')->truncate();
        DB::table('persistences')->truncate();
        DB::table('reminders')->truncate();
        DB::table('role_users')->truncate();
        DB::table('throttle')->truncate();

        array_map('unlink', glob(avatar_path('*')));
        $faker = \Faker\Factory::create();

        $avatar = $faker->image(avatar_path(), 128, 128);
        $avatar = explode(DIRECTORY_SEPARATOR, $avatar);
        $avatar = last($avatar);

        Sentinel::registerAndActivate([
            'avatar' => $avatar,
            'email' => 'admin@pratamakrida.co.id',
            'password' => '12345678',
            'first_name' => 'Super',
            'last_name' => 'Administrator',
            'is_admin' => true,
        ]);

        Sentinel::findRoleBySlug('super-admin')->users()->attach(Sentinel::findById(1));

        for ($i = 2; $i <= 5; $i++) {
            $avatar = $faker->image(avatar_path(), 128, 128);
            $avatar = explode(DIRECTORY_SEPARATOR, $avatar);
            $avatar = last($avatar);

            Sentinel::registerAndActivate([
                'avatar' => $avatar,
                'email' => $faker->freeEmail,
                'password' => '12345678',
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'is_admin' => true,
            ]);

            $rand = array_rand($this->roleSlugsArray);

            Sentinel::findRoleBySlug($this->roleSlugsArray[$rand])->users()->attach(Sentinel::findById($i));
        }
    }
}
