<?php

use App\Models\Hashtag;
use Illuminate\Database\Seeder;

class HashtagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('hashtags')->truncate();
        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 20; $i++) {
            Hashtag::create([
                'name' => $faker->word,
                'user_id' => 1,
                'type' => 'admin',
                'is_blocked' => false,
            ]);
        }
    }
}
