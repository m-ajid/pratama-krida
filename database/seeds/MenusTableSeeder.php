<?php

use App\Models\Menu;
use Illuminate\Database\Seeder;

class MenusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menus')->truncate();

        foreach ($this->getMenus() as $menu) {
            if ($menu['is_parent']) {
                $m = array_except($menu, 'child');

                $m = Menu::create($m);

                foreach ($menu['child'] as $child) {
                    $child = array_add($child, 'parent', $m->id);

                    Menu::create($child);
                }
            } else {
                Menu::create($menu);
            }
        }
    }

    /**
     * Get menus array.
     *
     * @return array
     */
    private function getMenus()
    {
        return [
            ['is_parent' => false, 'name' => str_slug('Dashboard'), 'display_name' => 'Dashboard', 'icon' => 'tachometer', 'href' => 'admin/dashboard', 'pattern' => 'dashboard'],

            ['is_parent' => true, 'name' => str_slug('User Trustee Management'), 'display_name' => 'User Trustee Management', 'icon' => 'users', 'href' => '#', 'pattern' => 'user-trustees', 'child' => [
                ['name' => str_slug('Role Management'), 'display_name' => 'Role Management', 'icon' => 'user-secret', 'href' => 'admin/user-trustees/roles', 'pattern' => 'user-trustees'],
                ['name' => str_slug('User Management'), 'display_name' => 'User Management', 'icon' => 'users', 'href' => 'admin/user-trustees/users', 'pattern' => 'user-trustees'],
                ['name' => str_slug('Menu Management'), 'display_name' => 'Menu Management', 'icon' => 'bar', 'href' => 'admin/user-trustees/menus', 'pattern' => 'user-trustees'],
            ]],

            ['is_parent' => true, 'name' => str_slug('Content Management'), 'display_name' => 'Content Management', 'icon' => 'pencil-square-o', 'href' => '#', 'pattern' => 'content-management', 'child' => [
                ['name' => str_slug('About Us'), 'display_name' => 'About Us', 'icon' => '', 'href' => 'admin/content/about-us', 'pattern' => 'content-management'],
                ['name' => str_slug('Our Service'), 'display_name' => 'Our Service', 'icon' => '', 'href' => 'admin/content/our-service', 'pattern' => 'content-management'],
                ['name' => str_slug('Our Project'), 'display_name' => 'Our Project', 'icon' => '', 'href' => 'admin/content/our-project', 'pattern' => 'content-management'],
                ['name' => str_slug('Testimony'), 'display_name' => 'Testimony', 'icon' => '', 'href' => 'admin/content/testimony', 'pattern' => 'content-management']
            ]],

            ['is_parent' => false, 'name' => str_slug('Gallery'), 'display_name' => 'Gallery', 'icon' => 'picture-o', 'href' => 'admin/gallery', 'pattern' => 'gallery'],
            ['is_parent' => false, 'name' => str_slug('Message'), 'display_name' => 'Message', 'icon' => 'envelope-o', 'href' => 'admin/message', 'pattern' => 'message'],
            ['is_parent' => false, 'name' => str_slug('Carousel'), 'display_name' => 'Carousel', 'icon' => 'picture-o', 'href' => 'admin/carousel', 'pattern' => 'carousel'],

            ['is_parent' => true, 'name' => str_slug('Settings'), 'display_name' => 'Settings', 'icon' => 'gears', 'href' => '#', 'pattern' => 'settings', 'child' => [
                ['name' => str_slug('Contact'), 'display_name' => 'Contact Us', 'icon' => '', 'href' => 'admin/settings/contact', 'pattern' => 'settings'],
                ['name' => str_slug('Background'), 'display_name' => 'Background', 'icon' => '', 'href' => 'admin/settings/background', 'pattern' => 'settings']
            ]],
            
        ];
    }
}