<?php

use App\Models\Directory;
use Illuminate\Database\Seeder;

class DirectoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('directories')->truncate();
        $faker = \Faker\Factory::create();

        for ($i = 1; $i <= 20; $i++) {
            Directory::create([
                'name' => $faker->company,
                'address' => $faker->address,
                'latitude' => $faker->latitude,
                'longitude' => $faker->longitude,
                'phone_number' => $faker->phoneNumber,
                'email' => $faker->freeEmail,
            ]);
        }
    }
}
