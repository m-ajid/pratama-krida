<?php

use Illuminate\Database\Seeder;
use App\Models\ContactUs;
class ContactsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data=[
            'company'=>'PRATAMA KRIDA',
            'address'=>'Bandung - Jawa Barat',
            'phone'=>'6282216912584',
            'mobile'=>'6282216912584',
            'email'=>'info@pratamakrida.co.id',
            'facebook'=>'#',
            'twitter'=>'#',
            'instagram'=>'#',
            'gplus'=>'#',
            'youtube'=>'#',
            'linked_in'=>'#',
            'map'=>'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3960.4795138111726!2d107.57789821477306!3d-6.952624594976941!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e68e8cbd5502deb%3A0xd7afee3afa778f2!2sJl.+Cibolerang+No.68%2C+Margasuka%2C+Babakan+Ciparay%2C+Kota+Bandung%2C+Jawa+Barat+40225!5e0!3m2!1sid!2sid!4v1494135093365',
            'image'=>'pratamakrida.png'
        ];
        DB::table('contact_uses')->truncate();
        $contact=ContactUs::create($data);
    }
}
