<?php

use Illuminate\Database\Seeder;

class AboutUsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('about_uses')->truncate();
        DB::table('about_uses')->insert([
            'short_description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed dapibus leo nec ornare diamcommodo nibh ante facilisis.',
            'image'=>'design.jpg',
            'description' =>'<h4>Who We Are</h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed dapibus leo nec ornare diam. Sed commodo nibh ante facilisis bibendum dolor feugiat at. Duis sed dapibus leo nec ornare diam commodo nibh ante facilisis bibendum.</p>
            <div class="space"></div>
            <h4>What We Do</h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed dapibus leo nec ornare diam. Sed commodo nibh ante facilisis bibendum dolor feugiat at.</p>
            <div class="space"></div><div class="list-style">
              <div class="row">
                <div class="col-lg-6 col-sm-6 col-xs-12">
                  <ul>
                    <li>Lorem ipsum dolor</li>
                    <li>Consectetur adipiscing</li>
                    <li>Duis sed dapibus leo</li>
                    <li>Sed commodo nibh ante</li>
                  </ul>
                </div>
                <div class="col-lg-6 col-sm-6 col-xs-12">
                  <ul>
                    <li>Lorem ipsum dolor</li>
                    <li>Consectetur adipiscing</li>
                    <li>Duis sed dapibus leo</li>
                    <li>Sed commodo nibh ante</li>
                  </ul>
                </div>
              </div>
            </div> ',
            'created_at'=>date('Y-m-d h:i:s'),
            'updated_at'=>date('Y-m-d h:i:s')
        ]);
    }
}
