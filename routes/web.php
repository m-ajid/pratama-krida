<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['namespace'=>'Frontend'], function(){
    Route::get('/', ['as'=>'home','uses'=>'HomeController@index']);
    Route::get('legalitas', ['as'=>'legalitas','uses'=>'LegalController@index']);
    Route::get('gallery', ['as'=>'gallery','uses'=>'GalleryController@index']);
    Route::get('service', ['as'=>'service','uses'=>'ServiceController@index']);
    Route::post('message/store',['as' => 'message.store', 'uses'=>'MessageController@store']);
});
// Route::group(function (){
//     Route::get('/filemanager', '\Unisharp\Laravelfilemanager\controllers\LfmController@show');
//     Route::post('/filemanager/upload', '\Unisharp\Laravelfilemanager\controllers\LfmController@upload');
// });
Route::group(['prefix' => 'auth', 'namespace' => 'Backend'], function () {
        Route::get('login', 'AuthController@getLogin');
        Route::post('login', 'AuthController@postLogin');
        Route::get('logout', 'AuthController@getLogout');
        Route::get('reset-password', 'AuthController@getResetPassword');
        Route::post('reset-password', 'AuthController@postResetPassword');
        Route::get('change-password', 'AuthController@getChangePassword');
        Route::post('change-password', 'AuthController@postChangePassword');
});

Route::group(['middleware' => 'sentinel_auth','prefix'=>'admin','namespace'=>'Backend'], function(){
    Route::group(['prefix' => 'datatables'], function () {
        Route::get('roles', 'UserTrustee\RoleController@datatables');
        Route::get('user', 'UserTrustee\UserController@datatables');
        Route::get('menu', 'UserTrustee\MenuController@datatables');
    });
    Route::get('/', function () {
        return redirect('admin/dashboard');
    });
    Route::get('dashboard',['as' => 'admin.dashboard', 'uses'=>'DashboardController@index']);
    Route::get('profile', 'ProfileController@index');
    Route::put('profile/{type}', 'ProfileController@update');

    Route::group(['prefix' => 'user-trustees', 'namespace' => 'UserTrustee'], function () {
        Route::resource('menus', 'MenuController', ['except' => 'show']);
        Route::resource('roles', 'RoleController', ['except' => 'show']);
        Route::resource('users', 'UserController', ['except' => 'show']);
    });
    Route::group(['prefix'=>'settings'],function(){
        Route::get('contact/',['as' => 'admin.settings.contact.edit', 'uses'=>'ContactController@Edit']);
        Route::patch('contact/update/{id}',['as' => 'admin.settings.contact.update', 'uses'=>'ContactController@update']);
        Route::get('background/',['as' => 'admin.settings.background.edit', 'uses'=>'SettingController@backgroundEdit']);
        Route::get('background/update/{id}',['as' => 'admin.settings.background.update', 'uses'=>'SettingController@backgroundUpdate']);
    });
    Route::group(['prefix'=>'gallery'],function(){
        Route::get('/',['as' => 'admin.gallery', 'uses'=>'GalleryController@index']);
        Route::post('upload',['as' => 'admin.gallery.upload', 'uses'=>'GalleryController@upload']);
        Route::get('create',['as' => 'admin.gallery.create', 'uses'=>'GalleryController@create']);
        Route::get('create-album',['as' => 'admin.gallery.create-album', 'uses'=>'GalleryController@createAlbum']);
        Route::post('store',['as' => 'admin.gallery.store', 'uses'=>'GalleryController@store']);
        Route::get('album/{album}',['as' => 'admin.gallery.album', 'uses'=>'GalleryController@album']);
        Route::get('edit/{id}',['as' => 'admin.gallery.edit', 'uses'=>'GalleryController@edit']);
        Route::patch('update/{id}',['as' => 'admin.gallery.update', 'uses'=>'GalleryController@update']);
        Route::delete('delete/{id}',['as' => 'admin.gallery.delete', 'uses'=>'GalleryController@delete']);
    });
    Route::group(['prefix'=>'content'],function(){
        Route::group(['prefix'=>'about-us'],function(){
            Route::get('/',['as' => 'admin.about', 'uses'=>'AboutController@index']);
            Route::patch('update/{id}',['as' => 'admin.about.update', 'uses'=>'AboutController@update']);
        });
        Route::group(['prefix'=>'our-service'],function(){
            Route::get('/',['as' => 'admin.service', 'uses'=>'ServiceController@index']);
            Route::get('create',['as' => 'admin.service.create', 'uses'=>'ServiceController@create']);
            Route::post('store',['as' => 'admin.service.store', 'uses'=>'ServiceController@store']);
            Route::get('edit/{id}',['as' => 'admin.service.edit', 'uses'=>'ServiceController@edit']);
            Route::patch('update/{id}',['as' => 'admin.service.update', 'uses'=>'ServiceController@update']);
            Route::delete('delete/{id}',['as' => 'admin.service.delete', 'uses'=>'ServiceController@delete']);
        });
        Route::group(['prefix'=>'our-project'],function(){
            Route::get('/',['as' => 'admin.project', 'uses'=>'ProjectController@index']);
            Route::get('create',['as' => 'admin.project.create', 'uses'=>'ProjectController@create']);
            Route::post('store',['as' => 'admin.project.store', 'uses'=>'ProjectController@store']);
            Route::get('edit/{id}',['as' => 'admin.project.edit', 'uses'=>'ProjectController@edit']);
            Route::patch('update/{id}',['as' => 'admin.project.update', 'uses'=>'ProjectController@update']);
            Route::delete('delete/{id}',['as' => 'admin.project.delete', 'uses'=>'ProjectController@delete']);
        });
        Route::group(['prefix'=>'testimony'],function(){
            Route::get('/',['as' => 'admin.testimony', 'uses'=>'TestimonyController@index']);
            Route::get('create',['as' => 'admin.testimony.create', 'uses'=>'TestimonyController@create']);
            Route::post('store',['as' => 'admin.testimony.store', 'uses'=>'TestimonyController@store']);
            Route::get('edit/{id}',['as' => 'admin.testimony.edit', 'uses'=>'TestimonyController@edit']);
            Route::patch('update/{id}',['as' => 'admin.testimony.update', 'uses'=>'TestimonyController@update']);
            Route::delete('delete/{id}',['as' => 'admin.testimony.delete', 'uses'=>'TestimonyController@delete']);
        });
        
    });
    Route::group(['prefix'=>'carousel'],function(){
            Route::get('/',['as' => 'admin.carousel', 'uses'=>'CarouselController@index']);
            Route::get('create',['as' => 'admin.carousel.create', 'uses'=>'CarouselController@create']);
            Route::post('store',['as' => 'admin.carousel.store', 'uses'=>'CarouselController@store']);
            Route::get('edit/{id}',['as' => 'admin.carousel.edit', 'uses'=>'CarouselController@edit']);
            Route::patch('update/{id}',['as' => 'admin.carousel.update', 'uses'=>'CarouselController@update']);
            Route::delete('delete/{id}',['as' => 'admin.carousel.delete', 'uses'=>'CarouselController@delete']);
        });
    Route::group(['prefix'=>'message'],function(){
        Route::get('/',['as' => 'admin.message', 'uses'=>'MessageController@index']);
        Route::get('create',['as' => 'admin.message.create', 'uses'=>'MessageController@create']);
        Route::post('store',['as' => 'admin.message.store', 'uses'=>'MessageController@store']);
        Route::get('edit/{id}',['as' => 'admin.message.edit', 'uses'=>'MessageController@edit']);
        Route::patch('update/{id}',['as' => 'admin.message.update', 'uses'=>'MessageController@update']);
        Route::delete('delete/{id}',['as' => 'admin.message.delete', 'uses'=>'MessageController@delete']);
    });
});